<?php

use yii\db\Migration;

/**
 * Class m191002_194742_add_new_columns_tables_temp_media_content
 */
class m191002_194742_add_new_columns_tables_temp_media_content extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('temp_photo', 'owner_id', 'string');
        $this->addColumn('temp_video', 'owner_id', 'string');
        $this->addColumn('temp_audio', 'owner_id', 'string');
        $this->addColumn('media_content_photo', 'owner_id', 'string');
        $this->addColumn('media_content_video', 'owner_id', 'string');
        $this->addColumn('media_content_audio', 'owner_id', 'string');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('temp_photo', 'owner_id');
        $this->dropColumn('temp_video', 'owner_id');
        $this->dropColumn('temp_audio', 'owner_id');
        $this->dropColumn('media_content_photo', 'owner_id');
        $this->dropColumn('media_content_video', 'owner_id');
        $this->dropColumn('media_content_audio', 'owner_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191002_194742_add_new_columns_tables_temp_media_content cannot be reverted.\n";

        return false;
    }
    */
}
