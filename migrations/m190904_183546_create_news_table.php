<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m190904_183546_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'text' => $this->string(),
            'author' => $this->string(),
            'date_create' => $this->date(),
        ]);
        $this->alterColumn('news', 'text', 'longtext');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('news');
    }
}
