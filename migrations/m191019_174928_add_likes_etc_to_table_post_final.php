<?php

use yii\db\Migration;

/**
 * Class m191019_174928_add_likes_etc_to_table_post_final
 */
class m191019_174928_add_likes_etc_to_table_post_final extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('post_final', 'likes', 'integer');
        $this->addColumn('post_final', 'comments', 'integer');
        $this->addColumn('post_final', 'reposts', 'integer');
        $this->addColumn('post_final', 'views', 'integer');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('post_final', 'likes');
        $this->dropColumn('post_final', 'comments');
        $this->dropColumn('post_final', 'reposts');
        $this->dropColumn('post_final', 'views');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191019_174928_add_likes_etc_to_table_post_final cannot be reverted.\n";

        return false;
    }
    */
}
