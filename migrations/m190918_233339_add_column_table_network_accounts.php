<?php

use yii\db\Migration;

/**
 * Class m190918_233339_add_column_table_network_accounts
 */
class m190918_233339_add_column_table_network_accounts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('network_accounts', 'application_id', 'integer');

        $this->addColumn('network_accounts', 'access_expires_in', 'integer');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('network_accounts', 'application_id');

        $this->dropColumn('network_accounts', 'access_expires_in');

        return true;

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190918_233339_add_column_table_network_accounts cannot be reverted.\n";

        return false;
    }
    */
}
