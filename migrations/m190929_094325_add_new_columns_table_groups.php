<?php

use yii\db\Migration;

/**
 * Class m190929_094325_add_new_columns_table_groups
 */
class m190929_094325_add_new_columns_table_groups extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('groups', 'vk_id', 'string');
        $this->addColumn('groups', 'vk_screen_name', 'string');
        $this->addColumn('groups', 'vk_photo_200', 'string');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('groups', 'vk_id');
        $this->dropColumn('groups', 'vk_screen_name');
        $this->dropColumn('groups', 'vk_photo_200');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190929_094325_add_new_columns_table_groups cannot be reverted.\n";

        return false;
    }
    */
}
