<?php

use yii\db\Migration;

/**
 * Class m190925_183342_add_column_vk_username_to_network_accounts
 */
class m190925_183342_add_column_vk_username_to_network_accounts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('network_accounts', 'vk_username', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('network_accounts', 'vk_username');

        return true;
    }

}
