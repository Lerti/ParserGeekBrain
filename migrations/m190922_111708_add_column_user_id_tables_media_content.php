<?php

use yii\db\Migration;

/**
 * Class m190922_111708_add_column_user_id_tables_media_content
 */
class m190922_111708_add_column_user_id_tables_media_content extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('media_content_photo', 'user_id', 'integer');
        $this->addColumn('media_content_video', 'user_id', 'integer');
        $this->addColumn('media_content_audio', 'user_id', 'integer');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('media_content_photo', 'user_id');
        $this->dropColumn('media_content_video', 'user_id');
        $this->dropColumn('media_content_audio', 'user_id');
    }
}
