<?php

use yii\db\Migration;

/**
 * Class m191002_191813_add_new_columns_tables_temp_photo_video_audio
 */
class m191002_191813_add_new_columns_tables_temp_photo_video_audio extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('temp_photo', 'vk_media_id', 'string');
        $this->addColumn('temp_video', 'vk_media_id', 'string');
        $this->addColumn('temp_audio', 'vk_media_id', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('temp_photo', 'vk_media_id');
        $this->dropColumn('temp_video', 'vk_media_id');
        $this->dropColumn('temp_audio', 'vk_media_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191002_191813_add_new_columns_tables_temp_photo_video_audio cannot be reverted.\n";

        return false;
    }
    */
}
