<?php

use yii\db\Migration;

/**
 * Class m190907_084541_correnction_media_text_colums
 */
class m190907_084541_correnction_media_text_colums extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->db->createCommand("ALTER TABLE {{%temp_photo}} MODIFY [[text]] longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci")->execute();

        $this->db->createCommand("ALTER TABLE {{%temp_audio}} MODIFY [[title]] longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci")->execute();

        $this->db->createCommand("ALTER TABLE {{%temp_video}} MODIFY [[title]] longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci")->execute();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190907_084541_correnction_media_text_colums cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190907_084541_correnction_media_text_colums cannot be reverted.\n";

        return false;
    }
    */
}
