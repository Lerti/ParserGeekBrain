<?php

use yii\db\Migration;

/**
 * Class m190921_134827_add_column_user_id_temp_photo_temp_audio_temp_video
 */
class m190921_134827_add_column_user_id_temp_photo_temp_audio_temp_video extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('temp_photo', 'user_id', 'integer');
        $this->addColumn('temp_video', 'user_id', 'integer');
        $this->addColumn('temp_audio', 'user_id', 'integer');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('temp_photo', 'user_id');
        $this->dropColumn('temp_video', 'user_id');
        $this->dropColumn('temp_audio', 'user_id');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190921_134827_add_column_user_id_temp_photo_temp_audio_temp_video cannot be reverted.\n";

        return false;
    }
    */
}
