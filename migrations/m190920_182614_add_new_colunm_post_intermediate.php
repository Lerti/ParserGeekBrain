<?php

use yii\db\Migration;

/**
 * Class m190920_182614_add_new_colunm_post_intermediate
 */
class m190920_182614_add_new_colunm_post_intermediate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('post_intermediate', 'user_id', 'integer');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('post_intermediate', 'user_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190920_182614_add_new_colunm_post_intermediate cannot be reverted.\n";

        return false;
    }
    */
}
