<?php

use yii\db\Migration;

/**
 * Class m191002_191408_add_new_columns_tables_media_content
 */
class m191002_191408_add_new_columns_tables_media_content extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('media_content_photo', 'vk_media_id', 'string');
        $this->addColumn('media_content_video', 'vk_media_id', 'string');
        $this->addColumn('media_content_audio', 'vk_media_id', 'string');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('media_content_photo', 'vk_media_id');
        $this->dropColumn('media_content_video', 'vk_media_id');
        $this->dropColumn('media_content_audio', 'vk_media_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191002_191408_add_new_columns_tables_media_content cannot be reverted.\n";

        return false;
    }
    */
}
