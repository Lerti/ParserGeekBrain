<?php

use yii\db\Migration;

/**
 * Class m191019_154528_add_likes_etc_to_table_post_intermediate
 */
class m191019_154528_add_likes_etc_to_table_post_intermediate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('post_intermediate', 'likes', 'integer');
        $this->addColumn('post_intermediate', 'comments', 'integer');
        $this->addColumn('post_intermediate', 'reposts', 'integer');
        $this->addColumn('post_intermediate', 'views', 'integer');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('post_intermediate', 'likes');
        $this->dropColumn('post_intermediate', 'comments');
        $this->dropColumn('post_intermediate', 'reposts');
        $this->dropColumn('post_intermediate', 'views');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191019_154528_add_likes_etc_to_table_post_intermediate cannot be reverted.\n";

        return false;
    }
    */
}
