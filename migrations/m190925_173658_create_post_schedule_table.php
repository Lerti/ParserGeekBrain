<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post_schedule`.
 */
class m190925_173658_create_post_schedule_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('post_schedule', [
            'id' => $this->primaryKey(),
            'post_final_id' => $this->integer(),
            'group_id' => $this->integer(),
            'user_id' => $this->integer(),
            'account_id' => $this->integer(),
            'dateTime' => $this->dateTime(),
            'status' => $this->smallInteger()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('post_schedule');
    }
}
