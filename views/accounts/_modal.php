<?php
/**
 * 25.09.2019
 * 22:01
 *
 * @var $errorCodeFromUser string
 */
use yii\bootstrap\Modal;
use yii\helpers\Html;
use app\assets\AccountAsset;


AccountAsset::register($this);
?>

<div>

    <p>Связать аккаунт с приложением, чтобы постить:</p>

    <div class="">

        <?php
        Modal::begin([
            'header' => '<p>1. Нажмите "Получить код".
</p><p>2. Подтвердите доступ приложения.</p><p>3. Cкопируйте адресную строку из новой вкладки в поле ниже. Нажмите кнопку "Сохранить". Копируйте адрес целиком.</p>',
            'toggleButton' => [
                'label' => 'Привязать аккаунт Вконтакте',
                'tag' => 'button',
                'class' => 'btn',
            ],
            'size' => 'modal-lg',
        ]); ?>

        <?= Html::img('https://i.ibb.co/8z6Xzfx/vk-code-page.png', ['width' => 600]) ?>
        <?= Html::a('Получить код!', '/accounts/get-code', ['class' => 'btn btn-primary', 'target' => '_blank', 'style' => 'margin-left: 70px']); ?>

        <hr>

        <?= Html::beginForm("/accounts/parse-code")?>

        <?= Html::input('text', 'code-input', '', ['id' => 'code-input-id']) ?>

        <?= Html::submitButton('Сохранить код', ['class' => 'btn btn-primary', 'name' => 'contact-button', 'id' => 'saveCodeBtn']) ?>

        <?= Html::endForm()?>

    </div>

    <?php Modal::end(); ?>

    <? if($errorCodeFromUser == 'length') { ?>
        <p id = 'modal-message'>Введенный адрес слишком короткий.</p> <?
    } else if ($errorCodeFromUser == 'value') {?>
        <p id = 'modal-message'>Введите, пожалуйста, правильный адрес.</p>
     <? } ?>

</div>