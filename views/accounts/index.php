<?php
/**
 * 23.09.2019
 * 21:39
 */

//does have any applications
//$userApplications

?>

<?php
use yii\bootstrap\Modal;
use yii\helpers\Html;

//var_dump($userAccounts, $userApplications);

if ($userApplications && $userAccounts) {
    echo $this->render('_accTable', [
            'provider' => $provider,
    ]);
} else if (!$userApplications) {
    echo $this->render('_appWarning');
} else if (!$userAccounts) {
    echo $this->render('_modal', [
        'errorCodeFromUser' => false,
    ]);
}

