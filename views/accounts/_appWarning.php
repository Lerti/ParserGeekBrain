<?php
/**
 * 24.09.2019
 * 0:16
 */

use yii\bootstrap\Modal;
use yii\helpers\Html;


?>

<div><h3>
        У вас еще не создано ни одного приложения.
    </h3>
    <p>Чтобы публиковать посты, вам нужно <?= Html::a('создать свое приложение', '/applications') ?>.</p>
    <div style="background-color: #eeeeee"><i>Я хочу <?= Html::a('продолжать парсить', '/source') ?> без возможности
            публиковать посты.</i></div>

    <div class="access-token-modal"><?= $this->render('_modal'); ?></div>

</div>




