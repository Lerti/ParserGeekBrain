<?php
/**
 * 24.09.2019
 * 0:04
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\grid\ActionColumn;

?>
<p>Account Table</p>

<div>
    <?= GridView::widget([
        'dataProvider' => $provider,
        'columns' => [
            /*'account_url:text:Аккаунт',*/
            [
                'label' => 'Аккаунт',
                'format' => 'raw',
                'value' => function($data){
                    return Html::a($data->vk_username, $data->account_url, ['target' =>'_blank']);
                },
            ],
            [
                'label' => 'Срок действия access token',
                'format' => 'text',
                'value' => function($data){

        if ($data->access_expires_in == 0) {
            return 'Бессрочный';
        } else {
            return date('Y-m-d', $data->access_expires_in);
        }
                },
            ],
            [
                'class' => ActionColumn::className(),
                'header'=>'Удалить аккаунт',
                'headerOptions' => ['width' => '80'],
                'template' => '{delete}',
                'buttons'  => [

                    'delete' => function($url, $data) {

                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $data['id']], [

                            'title' => 'Удалить аккаунт ' . $data['vk_username'], 'data-confirm' => 'Все ваши группы для публикации также удалятся. Удалить этот аккаунт?','data-method' => 'post']);


                    }
                ],
            ],
        ],
    ]) ?>


    <!--<div class="access-token-modal"><?/*= $this->render('_modal'); */?></div>-->
</div>


