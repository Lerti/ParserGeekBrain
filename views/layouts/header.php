<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini"> Парсер </span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <li class="dropdown messages-menu">
                <a href="/applications/instruction" >
                    <span >Инструкция</span>
                </a>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <?php if(!Yii::$app->user->isGuest):?>
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?= '/upload/user/' .
                            Yii::$app->user->identity->avatar ?>" class="img-circle user-image-small" alt="User Image"/>
                            <span class="hidden-xs"> <?= Yii::$app->user->identity->username ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?= '/upload/user/' .
                                Yii::$app->user->identity->avatar ?>" class="img-circle" alt="User Image" id="user-image-big"/>
                                <p>
                                    <?= Yii::$app->user->identity->username ?>
                                    <small> На сайте с <?= Yii::$app->formatter->asDate(Yii::$app->user->identity->created_at, 'short') ?></small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <?= Html::a(
                                        'Профиль',
                                        ['/user/profile/' , 'id' => Yii::$app->user->identity->id],
                                        ['class' => 'btn btn-default btn-flat']
                                    ) ?>
                                </div>
                                <div class="pull-right">
                                    <?= Html::a(
                                        'Выйти',
                                        ['/user/logout'],
                                        ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                    ) ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                <?php endif;?>
                <!-- User Account: style can be found in dropdown.less -->
            </ul>
        </div>
    </nav>
</header>

