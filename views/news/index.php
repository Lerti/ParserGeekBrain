<?php
use \yii\helpers\Html;
$this->title = 'Новости';

?>

<div class="form-group">
        <?= Yii::$app->user->can('NewsCreate') ?
            Html::a('Добавить новость', ['news/create'], ['class' => 'btn btn-success']):'' ?>
    </div>
<?php
/** @var \yii\data\ActiveDataProvider $dataProvider */
echo \yii\widgets\ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => function($model){
        return \app\widgets\NewsPreview::widget(['model' => $model]);
    },
    'summary' => false,
    'options' => [
        'class' => 'news-container'
    ],
])
?>