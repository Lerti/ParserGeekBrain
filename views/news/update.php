<?php
$this->title = 'Редактирование новости';
use \yii\widgets\ActiveForm;
use yii\helpers\Url;
use \yii\helpers\Html;

?>

<div class="news_one_main row" style="width: 50%">
    <?php $form = ActiveForm::begin(['action' => Url::to(['news/save', 'id' => $model->id])]); ?>
    <div class="col-lg-12">
        <?= $form->field($model, 'header')->textInput(['maxlength' => 100 ]) ?>
    </div>
    <div class="col-lg-12 news_text_area" style="min-height: 200px">
        <?= $form->field($model, 'text')->textarea(['minheight' => 2000]) ?>
    </div>
    <div class="col-lg-12">
        <?= $form->field($model, 'author')->textInput() ?>
    </div>

    <div class="col-lg-12">
        <?= $form->field($model, 'date_create')
            ->textInput(['type' => 'date'])
        ?>
    </div>
</div>
<?= Html::submitButton("Сохранить", ['class' => 'btn btn-success']); ?>
<? ActiveForm::end() ?>


