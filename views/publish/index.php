<?php

use yii\grid\GridView;
use yii\helpers\Html;
use app\assets\PostfinalAsset;
use \app\assets\PublishAsset;
use yii\grid\ActionColumn;
use \acerix\yii2\readmore\Readmore;
use \kartik\datetime\DateTimePicker;
use \yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use \yii\helpers\Url;


PostfinalAsset::register($this);
PublishAsset::register($this);
echo Readmore::widget();

?>

<div class="informing-text">
    <h4 class="informing-text-h4">Ваши сохраненные публикации</h4>

    <?php if ($addedPosts): ?>

        <?php

        $content = $addedPosts . ' были сохранены для редактирования и отправки в группы.';

        echo Html::tag('h4', $content, ['class' => 'informing-text-h4']) ?>
    <?php endif; ?>

</div>
<?= Html::button('Удалить все посты', ['class' => 'btn btn_posts', 'id' => 'reset-button', 'style' => 'margin-bottom: 20px']); ?>
<div class="form_toggle">
    <div class="form_toggle-item item-1">
        <input id="fid-1" type="radio" name="radio" value="off" checked>
        <label for="fid-1">Одиночный постинг</label>
    </div>
    <div class="form_toggle-item item-2">
        <input id="fid-2" type="radio" name="radio" value="on">
        <label for="fid-2">Массовый постинг</label>
    </div>
</div>
<div class="publish_mass_posting_box" style="display: none">
    <div class="publish_mass_posting_box__count">Выбрано <div id="checkbox_count"></div> из <div id="checkbox_count_max"></div> постов.</div>
    <?= Html::button('Выбрать все',
        ['class' => 'btn btn_posts all_check', 'id' => 'all_check_button', 'style' => 'margin-bottom: 20px']); ?>
    <?= Html::dropDownList('group', 'null', $groups, ['prompt' => 'Выберите группу...','id'=>'publish_id_group', 'style' => 'margin-bottom: 20px']); ?>
    <?= Html::dropDownList('interval', 'null',[
            '300' => '5 минут', '600' => '10 минут', '900' => '15 минут', '1200' => '20 минут', '1500' => '25 минут', '1800' => '30 минут',
            '3600' => '1 час', '5400' => '1,5 часа', '7200' => '2 часа', '10800' => '3 часа', '21600' => '6 часов', '43200' => '12 часов', '86400' => '1 день',
            '172800' => '2 дня', '259200' => '3 дня', '604800' => '7 дней',], ['prompt' => 'Выберите интервал времени...','id'=>'publish_interval', 'style' => 'margin-bottom: 20px']); ?>
    <?= Html::button('Опубликовать',
        ['class' => 'btn btn_posts', 'id'=> 'savePosts', 'style' => 'margin-bottom: 20px']); ?>

</div>

<div class="for-posts">

    <?php

    /** @var \yii\data\ActiveDataProvider $dataProvider */
    echo \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => function ($model) {
            return \app\widgets\PublishPreview::widget([
                'model' => $model
            ]);
        },
        'summary' => false,
        'options' => [
            'class' => 'publish_post_container'
        ],
    ])
    ?>
</div>
