<?php

/* @var $this yii\web\View */

use app\assets\SourceAsset;
use yii\helpers\Html;
use yii\widgets\Pjax;
use \acerix\yii2\readmore\Readmore;

SourceAsset::register($this);
echo Readmore::widget();
?>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (!$account) {
        $text = '<p>У вас пока нет авторизованного аккаунта. Вы можете парсить посты без возможности публикации.</p><p> Чтобы начать публиковать, ' . Html::a('создайте приложение', '/applications') . ' и ' . Html::a('авторизуйте свой аккаунт.', '/accounts') . '</p>';
        echo Html::tag('div', $text, ['class' => 'container']);
    }?>

    <?= Html::beginForm(['source/'], 'post', ['class' => 'id-group-form']) ?>

    <?/*= Html::dropDownList('vk-api', [], $selectAccount, [
        'text' => 'Выберите аккаунт',
        'class' => 'form-control',
    ]) */?>


    <?= Html::input('text', 'groupUrl', '',
        ['class' => 'id-group-input form-control', 'placeholder' => 'Скопируйте сюда url группы']) ?>

    <?= Html::dropDownList('selector', $selectors[100], $selectors, [
        'text' => 'Выберите количество постов',
        'class' => 'form-control',
    ]) ?>

    <?= Html::checkboxList('filter-posts', [], $checkboxesText, [
        'class' => 'custom-control custom-checkbox',
        'itemOptions' => [
            'class' => 'custom-control-input',
        ]]) ?>

    <div class = 'source-buttons'>
        <?= Html::submitButton('Получить посты', ['class' => 'submit btn btn_posts btn-outline-secondary']) ?>

        <?= Html::button('Выбрать все посты', ['class' => 'btn btn_posts',
            'id' => 'select-all-button']); ?>
        <br><br>
        <?= Html::button('Сохранить выбранные посты',
            ['class' => ['btn', 'btn_posts'], 'id' => 'savePosts']) ?>

        <?= Html::endForm() ?>

    </div>

    <div class="posts-grid-view">
        <?php
        if ($message) {

            if (is_numeric($message)) {
                $content = 'Спарсено постов: ' . $message;
            } else {
                $content = $message;
            }

            echo Html::tag('p', $content, ['class' => 'source-add-post-message']);

            Pjax::begin();
            /** @var \yii\data\ActiveDataProvider $dataProvider */
            echo \yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'id' => 'posts_id',
                'itemView' => function ($model) {
                    return \app\widgets\IntermediatePreview::widget([
                        'model' => $model,
                    ]);
                },
                'options' => [
                    'class' => 'intermediate_post_container'
                ],
            ]);
            Pjax::end();

            //TODO: fix pagination issue - do not accept get-parameters while changing page
           /* echo GridView::widget(
                [
                    'dataProvider' => $provider,
                    "id" => "grid",
                    'columns' => [
                        [
                            'class' => \yii\grid\CheckboxColumn::class,
                            'options' => [
                                'name' => 'posts',
                            ]
                        ],

                        'url_group',
                        'text',
                        [
                            'attribute' => 'temp_photo',
                            'label' => 'Изображение',
                            'format' => 'html',

                            'value' => function ($data) {
                                $photos = $data->photos;
                                foreach ($photos as $photoObject) {
                                    $photoSizes = $photoObject->getAttributes($photoObject->photoSizes);

                                    foreach ($photoSizes as $size) {
                                        if ($size) {
                                            $url = $size;
                                        }
                                    }
                                }
                                if (isset($url)) {
                                    return Html::img($url, ['alt' => 'image VK', 'width' => '200']);
                                } else {
                                    return 'Изображение отсутствует';
                                }

                            }
                        ],
                    ],
                ]
            );*/
        }

        ?>

    </div>

</div>
