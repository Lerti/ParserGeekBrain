<?php

use app\models\Applications;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\tables\Applications */

$this->title = 'Параметры';
if(!$hide){
    $this->params['breadcrumbs'][] = $this->title;
}

\yii\web\YiiAsset::register($this);
?>
<div class="app-view">

    <?php
    echo \app\widgets\CardWidget::widget([
        'model'=> $model
    ]);

    ?>

</div>

