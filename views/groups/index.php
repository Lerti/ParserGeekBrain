<?php
/**
 * 26.09.2019
 * 22:04
 */


if ($account) {

    echo $this->render('_groupChoice', [
        'providerNewGroups' => $providerNewGroups,
    ]);
} else {
    echo $this->render('_groupWarning');

}

