<?php
/**
 * 26.09.2019
 * 22:11
 */

use app\assets\GroupsAsset;
use yii\grid\GridView;
use yii\helpers\Html;

GroupsAsset::register($this);
?>

<?= GridView::widget([
    'dataProvider' => $providerNewGroups,
    'columns' => [
        [
            'class' => \yii\grid\CheckboxColumn::class,
            'checkboxOptions' => function ($model) {

                return ['value' => $model['id']];
            },
            'options' => [
                'name' => 'posts',
            ]
        ],

        [
            'label' => 'Выберите и сохраните группы, в которых хотите публиковать',
            'format' => 'raw',
            'value' => function ($data) {
                $url = 'https://vk.com/' . $data['screen_name'];
                return Html::a($data['name'], $url, ['target' => '_blank']);
            },
        ],
    ],
]); ?>

<?= Html::button('Сохранить группы', ['id' => 'save-groups', 'class' => 'btn btn-outline-secondary']) ?>
