<?php
/**
 * 29.09.2019
 * 14:34
 */

use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

?>


<?= GridView::widget([
    'dataProvider' => $provider,
    'columns' => [
        [
            'label' => 'Сохраненные группы для публикации',
            'format' => 'raw',
            'value' => function ($data) {

                // var_dump($data); die;
                $url = 'https://vk.com/' . $data['vk_screen_name'];
                return Html::a($data['title'], $url, ['target' => '_blank']);
            },
        ],
        [
            'class' => ActionColumn::className(),
            'template' => '{delete}',
            'header' => 'Удалить группу',
            'buttons' => [

                'delete' => function ($url, $model) {

                    //  var_dump($url, $model); die;

                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model['id']], [

                        'title' => 'Удалить группу ' . $model['title'], 'data-confirm' => 'Удалить эту группу?', 'data-method' => 'post']);

                }
            ],
        ],
    ],
]); ?>

<details>
    <summary>Выбрать еще группы для сохранения</summary>
    <?= $this->render('_groupChoice', [
        'providerNewGroups' => $providerNewGroups,
    ]);
    ?>
</details>
