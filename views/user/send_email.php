<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $title string */
/* @var $user \app\models\User */

$resetLink = Yii::$app->urlManager->createUrl([
    'user/send-email', 'id' => $user->id]);

$this->title = $title;
?>

<div>
    <p>Вы можете получить еще одно письмо для подтверждения email.</p>
    <?= Html::a(Html::encode('Получить новое письмо для подтверждения email.'), $resetLink) ?>
</div>
