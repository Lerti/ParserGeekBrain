
<?
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $title string */
/* @var $buttonName string */
/* @var $registration bool*/

$this->title = $title;
?>

<div class="user-create">

    <?= $this->render('_form', [
        'model' => $model,
        'buttonName' => $buttonName,
        'registration' => $registration,
    ]) ?>

</div>