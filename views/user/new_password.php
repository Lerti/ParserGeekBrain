<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\User */
/* @var $message string */

$this->title = $title;

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

?>

<div class="login-box">
    <div class="login-logo">
        <h2><b>Восстановить пароль</b></h2>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Введите email, под которым вы зарегистрированы на сайте. <br>На указанный адрес придет письмо со ссылкой для восстановления пароля.</p>

        <?php $form = ActiveForm::begin(['id' => 'restore-password-form', 'enableClientValidation' => true]); ?>

        <?= $form
            ->field($model, 'email', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('email')]) ?>

        <div class="text-center">
            <?= Html::submitButton('Получить', ['class' => 'btn btn-primary btn-flat', 'name' => 'restore-password-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
        <br>
        <p class="login-box-msg"> <?= $message ?></p>
    </div>
</div>
