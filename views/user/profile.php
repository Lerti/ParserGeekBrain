<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $title string */

$this->title = $title;
?>
<div class="user-view text-center">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            ['label' => 'Аватар',
                'value' => ('/upload/user/' . $model->avatar),
                'format' => ['image',['height' => '100']],
            ],
            'email',
        ],
    ]) ?>

</div>