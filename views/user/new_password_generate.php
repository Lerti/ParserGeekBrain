<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\User */
/* @var $title string */

$this->title = $title;

?>

<div class = 'site-signup'>

    <p> Пожалуйста, введите новый пароль: </p>

    <div class = 'row'>
        <div class = 'col-lg-5'>
            <?php $form = \yii\bootstrap\ActiveForm::begin([
                'id' => 'form-generate-password',
                'options' => ['enctype' => 'multipart/form-data'],
                'layout' => 'horizontal',
                'fieldConfig' => ['horizontalCssClasses' =>
                    ['label' => 'col-sm-2',]],
            ]); ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary',
                'name' => 'generate-password-button']) ?>

            <?php \yii\bootstrap\ActiveForm::end(); ?>
        </div>
    </div>
</div>
