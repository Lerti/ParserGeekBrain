<?php
use app\assets\ProfileAsset;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $buttonName string */
/* @var $registration bool */

ProfileAsset::register($this);
?>
<div class = 'site-signup'>

    <div class = 'row'>
        <div class = 'col-lg-5'>
            <?php $form = \yii\bootstrap\ActiveForm::begin([
                    'id' => 'form-signup',
                    'options' => ['enctype' => 'multipart/form-data'],
                    'layout' => 'horizontal',
                    'fieldConfig' => ['horizontalCssClasses' =>
                    ['label' => 'col-sm-2',]],
            ]); ?>

            <?= $form->field($model, 'username')->textInput() ?>

            <? Modal::begin([
                'toggleButton' => [
                        'label' => 'Выбрать аватар',
                    'tag' => 'button',
                    'class' => 'btn btn_posts',
                    ],
                ]);
            ?>
            <?= $form->field($model, 'selectedAvatar')->checkboxList(User::getDefaultAvatars(), ['encode' => false,
                'itemOptions' => ['class' => 'checkbox_avatar', 'hidden' => true]]) ?>
            <?php Modal::end(); ?>
            <p id = 'selectedAvatarName'>Файл не выбран.</p>
            <br><br>
            <div>
                <?= $form->field($model, 'avatar')->fileInput(['id' => 'upload-user-show']) ?>
            </div>
            <? if($registration) {?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'password')->passwordInput()?>
                <?= $form->field($model, 'password2')->passwordInput()?>
            <? } ?>

            <?= Html::submitButton($buttonName, ['class' => 'btn btn_posts btn_profile',
                'name' => 'signup-button', 'style' => 'margin-left: 21px;']) ?>

            <?php \yii\bootstrap\ActiveForm::end(); ?>
        </div>
    </div>
</div>