<?php
use yii\helpers\Html;
use app\assets\ProfileAsset;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $buttonName string */
/* @var $registration bool */

ProfileAsset::register($this);
?>
<div class = 'site-signup new-password'>

    <div class = 'row'>
        <div class = 'col-lg-5'>
            <?php $form = \yii\bootstrap\ActiveForm::begin([
                    'id' => 'form-signup',
                    'options' => ['enctype' => 'multipart/form-data'],
                    'layout' => 'horizontal',
                    'fieldConfig' => ['horizontalCssClasses' =>
                    ['label' => 'col-sm-2',]],
            ]); ?>

            <?= $form->field($model, 'password')->passwordInput()?>

            <?= $form->field($model, 'passwordNew')->passwordInput()?>

            <?= Html::submitButton('Изменить пароль', ['class' => 'btn btn_posts btn_password',
                'name' => 'password-button']) ?>

            <?php \yii\bootstrap\ActiveForm::end(); ?>
        </div>
    </div>
</div>