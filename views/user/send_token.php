<?php
use yii\helpers\Html;

/* @var $message string */

$this->title = $title;

?>

<div class="login-box">
    <div class="login-logo">
        <h2><b>Восстановить пароль</b></h2>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">На указанный email было отправлено письмо для восстановления пароля.</p>
    </div>
</div>
