<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.EmailVerificationTokenExpire' => 3600,
    'user.PasswordResetTokenExpire' => 3600,
    'supportEmail' => 'admin@parsernash.ru',
    'vkApiVersion' => '5.71',
    'vkRootUrl' => 'https://vk.com/',
    'vkServiceKey' => 'c2186839c2186839c2186839aac274c2f7cc218c21868399f6ba4d48cd0399000ca1660',
];
