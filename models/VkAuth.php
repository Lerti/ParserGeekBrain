<?php
/**
 * 18.09.2019
 * 22:43
 */

namespace app\models;


use app\models\tables\Groups;
use app\models\tables\NetworkAccounts;
use app\models\tables\PostFinal;
use VK\Client\VKApiClient;
use VK\OAuth\VKOAuth;
use yii\base\Model;

class VkAuth extends Model
{
    public $networkID = 1;
    public $redirect_uri = 'https://oauth.vk.com/blank.html';

    /*public function hasUserAccessToken($user)
    {
        $accountVK = NetworkAccounts::findAll(['user_id' => $user->id, 'id_network' => $this->networkID]);
        $userAppVK = Applications::findOne(['user_id' => $user->id, 'network_id' => $this->networkID]);

        if (!$accountVK['access_token']) {
            $this->getAccessCode();
        }
        return true;
    }*/

    public function linkForGetCode($applicationID)
    {
        //$oauth = new VKOAuth();

        $client_id = $applicationID;
        $scope = 'groups+offline+wall+photos+notes+audio+video+stats+docs';
        //$display = VKOAuthDisplay::PAGE;
        //$state = 'secret_state_code';

        /*example of success link:
        'https://oauth.vk.com/authorize?type=web_server&client_id=7148368&redirect_uri=https%3A%2F%2Foauth.vk.com%2Fblank.html&response_type=code&scope=email+groups+offline+wall+photos+notes+audio+video+stats+docs';
        */

        return $browser_url =
            'https://oauth.vk.com/authorize?type=web_server&client_id='
            . $client_id
            . '&redirect_uri='
            . $this->redirect_uri
            . '&response_type=code&scope='
            . $scope;


        /* result: 'https://oauth.vk.com/authorize?type=web_server&client_id=7148355&redirect_uri=https%3A%2F%2Foauth.vk.com%2Fblank.html&response_type=code&scope=groups+offline+wall+photos+notes+audio+video+stats+docs'*/

        /* built-in method generates access-token with low access: $oauth->getAuthorizeUrl(VKOAuthResponseType::CODE, $client_id, $this->redirect_uri, $display, $scope, $state);*/

    }

    public function getAccessToken($code, $application)
    {
        $oauth = new VKOAuth();
        $userID = \Yii::$app->getUser()->getId();

        $client_id = $application->app_id;
        $client_secret = $application->secret_key;

        $userVKData = $oauth->getAccessToken($client_id, $client_secret, $this->redirect_uri, $code);

        //if will be allowed to have many accounts in one social media - change search logic
        $account = NetworkAccounts::findOne([
            'id_network' => $this->networkID,
            'sm_id' => $userVKData['user_id'],
            'user_id' => $userID,
        ]);

        if (!$account) {

            $account = new NetworkAccounts();
        }

        $this->setAccountAttributes($account, $userID, $userVKData, $client_id);

        if ($account->save(false)) {
            \Yii::$app->session->setFlash('success', 'Аккаунт добавлен');
            \Yii::$app->response->redirect('/accounts');

        } else {
            \Yii::$app->session->setFlash('failure', 'Не удалось сохранить или обновить аккаунт. Попробуйте получить код еще раз.');
            \Yii::$app->response->redirect('/accounts');
        }

        /*$vk = new VKApiClient();
        $response = $vk->wall()->post($account->access_token, array(
            'message' => 'one more message',
            'owner_id' =>  '-186080195',
        ));*/

    }

    public function setAccountAttributes($account, $userID, $userVKData, $client_id)
    {
        $account->user_id = $userID;
        $account->access_token = $userVKData['access_token'];
        $account->id_network = $this->networkID;
        $account->sm_id = $userVKData['user_id'];
        $account->account_url = 'https://vk.com/id' . $userVKData['user_id'];
        $account->application_id = $client_id;
        $account->access_expires_in = $userVKData['expires_in'];

        $vkUser = ($this->getVKUsername($account->access_token, $account->sm_id))[0];
        $vkUserName = $vkUser['first_name'] . ' ' . $vkUser['last_name'];
        $account->vk_username = $vkUserName;

    }

    public function parseCode($urlFromVk)
    {
        $codeArray = explode('=', $urlFromVk);
        $codeWithState = $codeArray[1];
        return $code = explode('&', $codeWithState)[0];

        /*code example:

        https://oauth.vk.com/blank.html#code=114a215c9ac65abbeb&state=secret_state_code
        https://oauth.vk.com/blank.html#code=eee598cc932aef7a33

        */
    }

    public function getVKUsername($accessToken, $userID)
    {
        $vk = new VKApiClient();

        return $response = $vk->users()->get($accessToken, [
            'user_ids' => [$userID],
            'fields' => ['first_name', 'last_name'],
        ]);
    }

    public function getGroups($account)
    {
        $vk = new VKApiClient(\Yii::$app->params['vkApiVersion']);

        $result = $vk->groups()->get($account->access_token,
            [
                'user_id' => $account->sm_id,
                'extended' => 1,
                'filter' => 'moder',
            ]);

        return ($result['items']);
    }

    public function getGroupsById(array $groupIds, $accountVK)
    {
        $vk = new VKApiClient();

        $response = $vk->groups()->getById(
            $accountVK->access_token,
            [
                'group_ids' => $groupIds,
            ]);

        return $response;
    }

    public static function postImmediatelyVK($userID, $groupID, $finalPostID)
    {

        $accessToken = NetworkAccounts::displayAccessToken($userID);

        $groupToPost = Groups::find($groupID)
            ->where(['id' => $groupID])
            ->one();

        $post = PostFinal::find()
            ->where(['id' => $finalPostID])
            ->one();

        $photos = $post->photos;

        $attachments = '';

        if (!empty($photos)) {

            foreach ($photos as $photo) {
                $attachments .= 'photo'
                    . $photo->owner_id
                    . '_'
                    . $photo->vk_media_id
                    . ',';
            }
            /* string type
            "photo100172_166443618,photo66748_265827614"
            "<owner_id>_<media_id>,<owner_id>_<media_id>"*/
        }
        $attachments = rtrim($attachments, ',');

        $vk = new VKApiClient(\Yii::$app->params['vkApiVersion']);

        $vk->wall()->post($accessToken, [
            'owner_id' => '-' . $groupToPost->vk_id,
            'from_group' => 1,
            'message' => $post->text,
            'attachments' => [$attachments],
            /*'publish_date' => 0*/
        ]);
    }
}