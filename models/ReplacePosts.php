<?php
/**
 * 06.09.2019
 * 22:39
 */

namespace app\models;


use app\models\tables\MediaContentPhoto;
use app\models\tables\PostFinal;
use app\models\tables\PostIntermediate;
use app\models\tables\TempAudio;
use app\models\tables\TempPhoto;
use app\models\tables\TempVideo;
use yii\base\Model;

class ReplacePosts extends Model
{
    static $postsSaved = 0;

    public $relatedTables = [
        'temp_photo',
        'temp_video',
        'temp_audio',
    ];

    public function start($keyList)
    {
        $userID = \Yii::$app->getUser()->getId();

        $postsToKeep = $this->getPosts($keyList, $userID);


        if ($postsToKeep) {

            $this->reSavePosts($postsToKeep, $userID);
            $this->clearPostsIntermediateTable($userID);
            $this->clearRelatedMedia($userID);

            return static::$postsSaved;
        } else {
            //echo 'Nothing to keep';
            return false;
        }
    }

    public function getPosts($keyList, $userID)
    {
        $postsIntermediate = PostIntermediate::find()
            ->where(['id' => $keyList, 'user_id' => $userID])
            ->all();

        return $postsIntermediate;
    }

    public function reSavePosts($postsToKeep, $userID)
    {
        //calculates duplicates - if post already exists in `post_final`
        $savedPostsIds = (new PostFinal())->getUsersPostsForComparing($userID);

        foreach ($postsToKeep as $postIntermediate) {

            $postIntermediateIds = [
                $postIntermediate->id_vk,
                $postIntermediate->owner_id
            ];

            $skipPostFlag = 0;

            foreach ($savedPostsIds as $oneSavedPostId) {

                $arraysAreDifferent =
                    array_diff($oneSavedPostId, $postIntermediateIds);

                if (empty($arraysAreDifferent)) {
                    $skipPostFlag++;
                };
            }

            if ($skipPostFlag) {
                continue;
            }
            //end of duplicates section

            $postFinal = new PostFinal();
            //print_r($postFinal->attributes);
            $postFinal->attributes = $postIntermediate->attributes;
            $postFinal->id_user = $userID;
            $postFinal->likes = $postIntermediate->likes;
            $postFinal->comments = $postIntermediate->comments;
            $postFinal->reposts = $postIntermediate->reposts;
            $postFinal->views = $postIntermediate->views;


            if ($postFinal->save()) {

                static::$postsSaved++;
                $this->resaveMedia($userID, $postIntermediate->id, $postFinal->id);

            } else {
                echo 'Post with id ' . $postIntermediate->id . ' wasn\'t saved';
                return false;
                //TODO: throw an Exception
            }
        }
        return static::$postsSaved;
    }

    public function resaveMedia($userID, $bufferPostID, $lastInsertedID)
    {
        $this->resavePhoto($userID, $bufferPostID, $lastInsertedID);
        //TODO: add video and audio resaves
    }

    public function resavePhoto($userID, $bufferPostID, $finalPostID)
    {
        $photos = TempPhoto::find()
            ->where(['id_post_intermediate' => $bufferPostID, 'user_id' => $userID])
            ->all();

        if (!$photos) {
            return false;
        }

        $columns = array_keys($photos[0]->attributes);
        $columnsToCopy = array_diff($columns, ['id', 'id_post_intermediate']);

        foreach ($photos as $photo) {
            $finalPhoto = new MediaContentPhoto();

            //preparing array for mass assignment
            $attributesFromBuffer = [];
            foreach ($columnsToCopy as $columnName) {
                foreach ($photo as $attrName => $value) {
                    if ($attrName == 'id') {continue;}
                    if ($attrName == $columnName) {
                        $attributesFromBuffer[$attrName] = $value;
                    }
                }
            }

            $finalPhoto->attributes = $attributesFromBuffer;
            $finalPhoto->id_post_final = $finalPostID;
            $finalPhoto->vk_media_id = $photo['vk_media_id'];
            $finalPhoto->owner_id = $photo['owner_id'];
            $finalPhoto->user_id = $userID;

            if (!$finalPhoto->save(false)) {
                return false;
            }
        }
    }

    public function clearPostsIntermediateTable($userID)
    {
        PostIntermediate::deleteAll(['user_id' => $userID]);
    }

    public function clearRelatedMedia($userID)
    {
        TempPhoto::deleteAll(['user_id' => $userID]);
        TempVideo::deleteAll(['user_id' => $userID]);
        TempAudio::deleteAll(['user_id' => $userID]);

    }
}