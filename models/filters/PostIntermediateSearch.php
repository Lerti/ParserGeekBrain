<?php

namespace app\models\filters;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\tables\PostIntermediate;

/**
 * PostIntermediateSearch represents the model behind the search form of `app\models\tables\PostIntermediate`.
 */


class PostIntermediateSearch extends PostIntermediate
{
    public $temp_video;
    public $temp_audio;
    public $temp_photo;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'marked_as_ads'], 'integer'],
            [['text', 'tags', 'url_group', 'id_vk', 'owner_id', 'date', 'post_type', 'id_group', 'temp_photo', 'temp_audio', 'temp_video'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PostIntermediate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'marked_as_ads' => $this->marked_as_ads,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['like', 'url_group', $this->url_group])
            ->andFilterWhere(['like', 'id_vk', $this->id_vk])
            ->andFilterWhere(['like', 'owner_id', $this->owner_id])
            ->andFilterWhere(['like', 'date', $this->date])
            ->andFilterWhere(['like', 'post_type', $this->post_type])
            ->andFilterWhere(['like', 'id_group', $this->id_group]);

        return $dataProvider;
    }
}
