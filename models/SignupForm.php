<?php
namespace app\models;
use Yii;
use yii\base\Model;
use app\models\User;
use yii\web\UploadedFile;
use yii\swiftmailer\Mailer;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $id;
    public $username;
    public $email;
    public $password;
    public $password2;
    public $selectedAvatar;

    /**
     * @var UploadedFile
     */
    public $avatarImg;
    public $avatar;

    const AVATAR_DEFAULT = 'avatar.jpg';

    public function rules()
    {
        return [
            [['username', 'email'], 'trim'],
            [['username', 'email', 'password', 'password2'], 'required'],

            ['username', 'unique', 'targetClass' => '\app\models\User',
                'message' => 'Пользователь с таким логином уже зарегистрирован.'],
            ['username', 'string', 'min' => 4, 'max' => 255],

            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User',
                'message' => 'Пользователь с таким email уже зарегистрирован.'],

            ['password', 'string', 'min' => 6, 'max' => 255],
            ['password2', 'compare', 'compareAttribute' => 'password'],

            [['avatar'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, png, jpeg',
                'maxSize' => 30 * 1024, 'tooBig' => 'Максимальный размер аватара 30KB'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'avatar' => 'Аватар',
            'password' => 'Пароль',
            'password2' => 'Повторите пароль',
            'selectedAvatar' => '',
        ];
    }

    public function getSelectedAvatar()
    {
        return $this->selectedAvatar;
    }

    public function setSelectedAvatar($selectedAvatar)
    {
        $this->selectedAvatar = $selectedAvatar;
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $this->avatar = UploadedFile::getInstance($this,'avatar');

        $user = new User();
        $user->id = $this->id;
        $user->username = $this->username;
        if(is_null($this->avatar)) {
            if($this->selectedAvatar != '') {
                $user->avatar = 'default_avatar/avatar' . $this->selectedAvatar . '.jpg';
            } else {
                $user->avatar = self::AVATAR_DEFAULT;
            }
        } else {
            $userAvatar = (string)User::getMaxId() . '_user_avatar.' . $this->avatar->getExtension();
            $user->avatarImg = $this->avatar;
            $user->avatar =  $userAvatar;
        }

        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        self::upload($user->avatar, $user->avatarImg);
        return $user->save() && $this->sendEmail($user);
    }

    public static function upload($name, $img)
    {
        if(!is_null($img)) {
            $path =  Yii::getAlias('@app/web') . '/upload/user/' . $name;
            $img->saveAs($path);
        }
        return true;
    }

    /**
     * Sends an email with a link, for verification email.
     * @var $user \app\models\User
     * @return bool whether the email was send
     */
    public static function sendEmail($user)
    {
        if (!User::isEmailVerificationTokenValid($user->verification_token)) {
            $user->generateEmailVerificationToken();
            if (!$user->save()) {
                return false;
            }
        }

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'ParserNash'])
            ->setTo($user->email)
            ->setSubject('Подтверждение регистрации ')
            ->send();
    }
}