<?php
/**
 * 29.08.2019
 * 20:29
 */

namespace app\models;

use app\models\tables\NetworkAccounts;
use app\models\tables\PostIntermediate;
use app\models\tables\TempPhoto;
use VK\Client\VKApiClient;
use yii\base\Model;
use app\models\tables\PostFinal;

class Source extends Model
{
    public $groupID;
    public $maxNumber = 100;

    //this model is oriented only for VK for now, so has VK id in attribute
    public $socialNetwork = 1;

    public $filterCheckboxes = [
        'noReposts' => 'Без репостов',
        'noMedia' => 'Пропустить посты без фото и видео',
        'noLinks' => 'Не включать посты со ссылками',
        'noAds' => 'Пропустить рекламу',
    ];

    static public $postsCount = 0;
    static public $repostsCount = 0;
    static public $alreadyExists = 0;
    static public $notSaved = 0;


    public function generatePostsNumber()
    {
        $maxNumber = $this->maxNumber;
        $minNumber = 10;
        $arrValues[0] = ['Выберите количество публикаций'];

        while ($minNumber <= $maxNumber) {
            $arrValues[$minNumber] = $minNumber;
            $minNumber += 10;
        }
        return $arrValues;
    }

    public function getVkGroupShortName($url)
    {
        $separatedUrl = explode('/', $url);
        return end($separatedUrl);

    }

    public function getPosts($groupUrl, $filterOptions, $userID, $accountVK, $postsNumber = 100)
    {
        $post = \Yii::$app->request->post();

        if (empty($post['groupUrl'])) {
            //TODO throw an Exception
            return 'Нужно ввести адрес группы или сообщества';
        }

        $groupShortName = $this->getVkGroupShortName($groupUrl);

        if ($accountVK) {
            $response = $this->getPostsViaSDK($accountVK, $groupShortName, $postsNumber);

        } else {
            $dataViaServiceKey = $this->getPostsViaServiceKey($postsNumber, $groupShortName);
            $response = $dataViaServiceKey['response'];
        }

        $message = $this->savePostsToDb($response, $groupShortName, $filterOptions, $userID);

        return $message;
    }

    public function savePostsToDb($postsArray, $groupID, $filterOptions, $userID)
    {
        $posts = $postsArray['items'];

        if (!is_array($posts)) {

            return 'Не удалось получить посты';
        }

        foreach ($posts as $key => $onePost) {

            if (is_array($filterOptions)) {
                if (!$this->applyUserFilter($onePost, $filterOptions)) {
                    continue;
                }
            }

            $twoPost = false;
            if (isset($onePost['copy_history'])) {
                $twoPost = $onePost;
                $onePost = $onePost['copy_history'][0];
                if (is_array($filterOptions)) {
                    if (!$this->applyUserFilter($onePost, $filterOptions)) {
                        continue;
                    }
                }
            }

            $model = new PostIntermediate();
            $model->user_id = $userID;
            $model->text = $onePost['text'];
            $model->id_group = $groupID;
            $model->url_group = 'https://vk.com/' . $groupID;
            $model->id_vk = $onePost['id'];
            $model->owner_id = $onePost['owner_id'];
            $model->date = $onePost['date'];
            $model->marked_as_ads = $onePost['marked_as_ads'];
            $model->post_type = $onePost['post_type'];

            if($twoPost) {
                $model->likes = $twoPost['likes']['count'];
                $model->comments = $twoPost['comments']['count'];
                $model->reposts = $twoPost['reposts']['count'];
                $model->views = $twoPost['views']['count'];
            } else {
                $model->likes = $onePost['likes']['count'];
                $model->comments = $onePost['comments']['count'];
                $model->reposts = $onePost['reposts']['count'];
                $model->views = $onePost['views']['count'];
            }
            //print_r($model->attributes);


            if ($model->save(false)) {
                static::$postsCount++;

            } else {
                //TODO throw an Exception
                static::$notSaved++;

            }

            if (isset($onePost['attachments'])) {

                foreach ($onePost['attachments'] as $attachment) {

                    if ($attachment['type'] == 'photo') {

                        $imageProps = $attachment['photo'];

                        $photo = new TempPhoto();
                        $photo->id_post_intermediate = $model->id;
                        $photo->user_id = $userID;
                        $photo->vk_media_id = $imageProps['id'];

                        if (!$this->savePhotos($photo, $imageProps)) {
                            //TODO throw an Exception
                            return 'Some photos from post\'s haven\'t been saved.';
                        }
                    }

                    if ($attachment['type'] == 'video') {
                        //TODO save video when access will change
                    }
                }
            }

        }
        return static::$postsCount;
    }

    public function savePhotos(TempPhoto $tempPhoto, $imageProps)
    {
        $tableColumns = array_keys($tempPhoto->attributes);
        $tableColumnsNoId = array_diff($tableColumns, ['id', 'user_id', 'vk_media_id']);

        foreach ($imageProps as $key => $value) {
            foreach ($tableColumnsNoId as $prop) {

                if ($key == $prop) {
                    $tempPhoto[$prop] = $value;
                }
            }
        }

        if ($tempPhoto->save(false)) {
            return true;
        } else {
            return false;
        }
    }

    public function displaySelectAccount($user)
    {
        $userAccounts = $user->accounts;
        $oneNetworkAccounts = [];

        foreach ($userAccounts as $account) {
            if ($account->id_network == $this->socialNetwork) {

                $oneNetworkAccounts[] = $account->account_url;
            }
        }

        return $oneNetworkAccounts;
    }

    public function applyUserFilter($onePost, $filterOptions)
    {

        //check up for no attachments - there are no photo and video
        if (!isset($onePost['attachments'])
            && in_array('noMedia', $filterOptions)) {
            return false;
        }

        //check up for advertisement
        if (isset($onePost['marked_as_ads'])) {

            if ($onePost['marked_as_ads'] == 1 && in_array('noAds', $filterOptions)) {
                return false;
            }
        }

        // check up for reposts
        if (isset($onePost['copy_history'])
            && in_array('noReposts', $filterOptions)) {
            return false;
        }

        $attachments = $onePost['attachments'];

        if (!is_array($attachments)) {
            //it's a repost when it is allowed by user
            return true;
        }

        $withPhoto = 0;
        $withVideo = 0;

        foreach ($attachments as $attachment) {

            // check up for posts with link
            if ($attachment['type'] == 'link'
                && in_array('noLinks', $filterOptions)) {
                return false;
            }

            // if post's attachments have video or photo, change flags
            if ($attachment['type'] == 'video') {
                $withVideo++;
            }

            if ($attachment['type'] == 'photo'
                || $attachment['type'] == 'posted_photo') {
                $withPhoto++;
            }
        }

        // check up for posts without photos and videos
        if (!($withPhoto + $withVideo)
            && in_array('noMedia', $filterOptions)) {
            return false;
        }

        return true;
    }

    public function getPostsViaSDK ($accountVK, $domain, $postsNumber) {

        $vk = new VKApiClient(\Yii::$app->params['vkApiVersion']);
        // TODO: if necessary reformat code for saving content for the newest version of API 5.101

        return $vk->wall()->get($accountVK->access_token,[
            'domain' => $domain,
            'count' => $postsNumber,
            'filter' => 'all',
        ]);
    }

    public function getPostsViaServiceKey($postsNumber, $groupShortName)
    {
        $params = [
            'domain' => $groupShortName,
            'offset' => '0',
            'count' => $postsNumber,
            'filter' => 'all',
            'access_token' => \Yii::$app->params['vkServiceKey'],
            'v' => \Yii::$app->params['vkApiVersion'],
        ];

        $url = 'https://api.vk.com/method/wall.get?' . http_build_query($params);

        //TODO: add an Exception handling vk errors in response
        return json_decode(file_get_contents($url), true);
    }
}