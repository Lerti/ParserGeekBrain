<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;

use app\models\tables\NetworkAccounts;
use app\models\tables\Applications;
//use mohorev\file\UploadImageBehavior;
use yii\helpers\FileHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "user".
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $avatar
 * @property UploadedFile $avatarImg
 * @property integer $status
 * @property string $accounts
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $applications
 *
 */

class User extends ActiveRecord implements IdentityInterface
{
    private $password;
    private $passwordNew;
    private $avatarImg;

    private $selectedAvatar;

    const SCENARIO_UPDATE_LOGIN = 'update_login';
    const SCENARIO_UPDATE_EMAIL = 'update_email';
    const SCENARIO_UPDATE_PASSWORD = 'update_password';
    const SCENARIO_RESTORE_PASSWORD = 'restore_password';
    const SCENARIO_GENERATE_PASSWORD = 'generate_password';

    const STATUS_DELETED = 0;
    const STATUS_NOT_ACTIVE = 1;
    const STATUS_ACTIVE = 10;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public function rules()
    {
        return [
            [['username', 'email'], 'trim'],

            [['username'], 'unique', 'targetClass' => '\app\models\User',
                'message' => 'This username has already been taken.', 'on' =>[self::SCENARIO_UPDATE_LOGIN]],
            ['username', 'string', 'min' => 4, 'max' => 255],

            ['status', 'default', 'value' => self::STATUS_NOT_ACTIVE],

            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'required', 'on' => [self::SCENARIO_RESTORE_PASSWORD]],
            [['email'], 'unique', 'targetClass' => '\app\models\User',
                'message' => 'This email address has already been taken.', 'on' =>[self::SCENARIO_UPDATE_EMAIL]],

            [['passwordNew', 'password'], 'required', 'on' => [self::SCENARIO_UPDATE_PASSWORD]],
            ['password', 'string', 'min' => 6, 'max' => 255, 'on' => [self::SCENARIO_UPDATE_PASSWORD]],
            ['password', 'validateOldPassword', 'on' => [self::SCENARIO_UPDATE_PASSWORD] ],
            ['passwordNew', 'string', 'min' => 6, 'max' => 255, 'on' => [self::SCENARIO_UPDATE_PASSWORD]],

            ['password', 'required', 'on' => [self::SCENARIO_GENERATE_PASSWORD]],
            ['password', 'string', 'min' => 6, 'max' => 255, 'on' => [self::SCENARIO_GENERATE_PASSWORD]],

            [['avatar'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, png, jpeg',
                'maxSize' => 30 * 1024, 'tooBig' => 'Максимальный размер аватара 30KB'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'avatar' => 'Загрузить аватар',
            'password' => 'Пароль',
            'auth_key' => 'Auth key',
            'password_hash' => 'Password hash',
            'password_reset_token' => 'Password reset token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created at',
            'updated_at' => 'Updated at',
            'verification_token' => 'Verification token',
            'passwordNew' => 'Новый пароль',
            'selectedAvatar' => '',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    /* функция из модели User по умолчанию от Yii
     * public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }
        return null;
    }*/

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by id
     *
     * @param int $id
     * @return static|null
     */
    public static function findById($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getPasswordNew()
    {
        return $this->passwordNew;
    }

    public function getSelectedAvatar()
    {
        return $this->selectedAvatar;
    }

    public function setSelectedAvatar($selectedAvatar)
    {
        $this->selectedAvatar = $selectedAvatar;
    }

    public function setPasswordNew($passwordNew)
    {
        $this->passwordNew = $passwordNew;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getAvatarImg()
    {
        return $this->avatarImg;
    }

    public function setAvatarImg($avatarImg)
    {
        $this->avatarImg = $avatarImg;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     * @param string $password
     * @throws
     */
    public function setPassword($password)
    {
        $this->password = $password;
        if($password) {
            $this->password_hash = Yii::$app->security->generatePasswordHash($password);
        }
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new email verification token
     */
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Finds user by email verification token
     *
     * @param string $token
     * @return static|null
     */
    public static function findByEmailVerificationToken($token)
    {
       return static::findOne([
           'verification_token' => $token,
       ]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        return static::findOne([
            'password_reset_token' => $token,
        ]);
    }

    /**
     * Define is email verification token valid
     *
     * @param string $token
     * @return static|false
     */
    public static function isEmailVerificationTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.EmailVerificationTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Define is password reset token valid
     *
     * @param string $token
     * @return static|false
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.PasswordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Validate email.
     * @return bool if email was validated
     */
    public function validateEmail()
    {
        $this->status = self::STATUS_ACTIVE;
        return $this->save(false);
    }

    /**
     * @param bool $insert whether this method called while inserting a record.
     * If `false`, it means the method is called while updating a record.
     * @return bool whether the insertion or updating should continue.
     * If `false`, the insertion or updating will be cancelled.
     **/
    public function beforeSave($insert)
    {
        if(!parent::beforeSave($insert)) {
            return false;
        }
        if($insert) {
            $this->generateAuthKey();
        }
        return true;
    }

    /**
     * Delete user account
     * @param integer $id
     * @return true
     */
    public function deleteUser($id) {

        $user = $this->findIdentity($id);

        $user->status = self::STATUS_DELETED;
        $user->save();

        return true;
    }

    /**
     * Select changed user information.
     * @var array $oldUser[]
     * @return string
     */
    public function selectChange($newUser)
    {
        $myNewUser = new User;

        if($this->username !== $newUser[username]) {
            $myNewUser->scenario = self::SCENARIO_UPDATE_LOGIN;
            $myNewUser->username = $newUser[username];
        } else {
            $myNewUser->username = $this->username;
        }

        if (!$myNewUser->validate()) {
            return $myNewUser;
        } else {
            return 'success';
        }
    }

    /**
     * Update user information.
     * @var array $newUser[]
     * @var string $userImage
     * @return string
     */
    public function updateUser ($newUser, $userImage) {
        $this->username = $newUser['username'];

        if($userImage !== '') {
            $this->updateAvatar();
        } else if ($newUser['selectedAvatar'] !== '') {
            $this->updateSelectedAvatar($newUser['selectedAvatar'][0]);
        }
        return $this->save();
    }

    /**
     * Update user avatar for the loaded avatar.
     */
    public function updateAvatar() {
        $this->avatarImg = UploadedFile::getInstance($this,'avatar');

        if(!is_null($this->avatar)) {
            $path =  Yii::getAlias('@app/web') . '/upload/user/' .
                $this->id . '_user_avatar.' . $this->avatarImg->getExtension();
            $this->avatarImg->saveAs($path);
            $this->avatar = $this->id . '_user_avatar.' . $this->avatarImg->getExtension();
        }
    }

    /**
     * Update user avatar for the selected avatar.
     * @param $avatar int
     */
    public function updateSelectedAvatar($avatar) {
        $this->avatar = 'default_avatar/avatar' . $avatar . '.jpg';
    }

    public function getAccounts()
    {
        return $this->hasMany(NetworkAccounts::className(), ['user_id' => 'id']);

    }

    public function getApplications()
    {
        return $this->hasMany(Applications::class, ['user_id' => 'id']);
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     */
    public function validateOldPassword($attribute)
    {
        $user = $this->findByUsername(Yii::$app->user->identity->username);;

        if (!$user->validatePassword($this->password)) {
            $this->addError($attribute, 'Введен неверный пароль.');
        }
    }

    /**
     * Update user password.
     * @var $id int
     * @return User
     */
    public function updatePassword ($id) {
        $user = $this->findIdentity($id);
        $user->setPassword($this->passwordNew);
        $user->save();
        return $user;
    }

    /**
     * Searches max id in bd 'user'
     * @return int
     */
    public static function getMaxId() {
        $maxId = User::find()->max('id');
        return $maxId + 1;
    }

    /**
     * Sends an email with a link, for restore password.
     * @var $user \app\models\User
     * @return bool whether the email was send
     */
    public static function sendPasswordEmail($user)
    {
        if (!self::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        } else {
            return false;
        }

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'restorePassword-html', 'text' => 'restorePassword-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'ParserNash'])
            ->setTo($user->email)
            ->setSubject('Изменение пароля ')
            ->send();
    }

    public static function getDefaultAvatars() {
        $dir = 'upload/user/default_avatar/';
        $avatars_array = FileHelper::findFiles($dir);
        $avatars_names = array_map(function ($avatars_array) {
            return '/upload/user/default_avatar/' . basename($avatars_array);
        },$avatars_array);

        foreach ($avatars_names as $key => $avatar) {
            $avatars_names[$key] = '<img src= "' . $avatar . '" class = "avatar-img" alt = "' . $avatar . '">' ;
        };

        return $avatars_names;
    }
}
