<?php

namespace app\models\tables;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "post_final".
 *
 * @property string $text
 * @property int $id
 * @property string $tags
 * @property int $status
 * @property int $id_user
 * @property string $id_vk
 * @property string $owner_id
 * @property string $date
 * @property int $marked_as_ads
 * @property string $post_type
 * @property string $id_group
 * @property string $photos
 * @property string $videos
 * @property string $audios
 * @property int $likes
 * @property int $comments
 * @property int $reposts
 * @property int $views
 */
class PostFinal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post_final';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['status', 'id_user', 'marked_as_ads'], 'integer'],
            [['tags', 'id_vk', 'owner_id', 'date', 'post_type', 'id_group'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'text' => 'Text',
            'id' => 'ID',
            'tags' => 'Tags',
            'status' => 'Status',
            'id_user' => 'Id User',
            'id_vk' => 'Id Vk',
            'owner_id' => 'Owner ID',
            'date' => 'Date',
            'marked_as_ads' => 'Marked As Ads',
            'post_type' => 'Post Type',
            'id_group' => 'Id Group',
            'likes' => 'likes',
            'comments' => 'comments',
            'reposts' => 'reposts',
            'views' => 'views',
        ];
    }

    public function getPhotos()
    {
        return $this->hasMany(MediaContentPhoto::class, ['id_post_final' => 'id']);

    }

    public function providerFinalPosts()
    {
        $user = Yii::$app->user->identity->getId();
        $query = $this->find();
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_ASC,
                    ]
                ],
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]
        );
        $query->filterWhere(['id_user' => $user]);
        return $dataProvider;
    }

    public function getUsersPostsForComparing($userID)
    {
        return static::find()
            ->where(['id_user' => $userID])
            ->select('id_vk, owner_id')
            ->asArray()
            ->all();

    }

}
