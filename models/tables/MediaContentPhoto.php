<?php

namespace app\models\tables;

use Yii;

/**
 * This is the model class for table "media_content_photo".
 *
 * @property int $id
 * @property int $id_post_final
 * @property string $photo_75
 * @property string $photo_130
 * @property string $photo_604
 * @property string $photo_807
 * @property string $photo_1280
 * @property string $photo_2560
 * @property int $width
 * @property int $height
 * @property string $text
 * @property string $vk_media_id
 * @property string $owner_id
 */
class MediaContentPhoto extends \yii\db\ActiveRecord
{
    public $photoSizes = ['photo_75', 'photo_130', 'photo_604', 'photo_807', 'photo_1280', 'photo_2560'];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'media_content_photo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_post_final', 'width', 'height'], 'integer'],
            [['photo_75', 'photo_130', 'photo_604', 'photo_807', 'photo_1280', 'photo_2560', 'text'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_post_final' => 'Id Post Final',
            'photo_75' => 'Photo 75',
            'photo_130' => 'Photo 130',
            'photo_604' => 'Photo 604',
            'photo_807' => 'Photo 807',
            'photo_1280' => 'Photo 1280',
            'photo_2560' => 'Photo 2560',
            'width' => 'Width',
            'height' => 'Height',
            'text' => 'Text',
        ];
    }
}
