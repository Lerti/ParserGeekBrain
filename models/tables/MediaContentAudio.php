<?php

namespace app\models\tables;

use Yii;

/**
 * This is the model class for table "media_content_audio".
 *
 * @property int $id
 * @property int $id_post_final
 * @property string $artist
 * @property string $title
 * @property int $duration
 * @property string $url
 */
class MediaContentAudio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'media_content_audio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_post_final', 'duration'], 'integer'],
            [['artist', 'title', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_post_final' => 'Id Post Final',
            'artist' => 'Artist',
            'title' => 'Title',
            'duration' => 'Duration',
            'url' => 'Url',
        ];
    }
}
