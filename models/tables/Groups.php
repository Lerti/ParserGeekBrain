<?php

namespace app\models\tables;

use app\models\VkAuth;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "groups".
 *
 * @property int $id
 * @property string $title
 * @property string $url
 * @property int $user_id
 * @property int $id_account
 * @property int $vk_id
 * @property int $vk_screen_name
 * @property int $vk_photo_200
 */
class Groups extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'groups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'id_account'], 'integer'],
            [['title', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
            'user_id' => 'User ID',
            'id_account' => 'Id Account',
        ];
    }

    public function saveUserGroups($groupIdList, $userID)
    {
        $accountVK = NetworkAccounts::getVKAccount($userID);

        $vkApi = new VkAuth();
        $groupsChecked = $vkApi->getGroupsById($groupIdList, $accountVK);
        $savedGroups = $this->getSavedGroups($accountVK->id, $userID);

        $savedGroupsId = array_column($savedGroups, 'vk_id');
        $idGroupsToSave = array_diff($groupIdList, $savedGroupsId);

        $countSavedGroups = 0;
        $countNotSavedGroups = 0;

        if (!empty($idGroupsToSave)) {
            foreach ($groupsChecked as $oneGroup) {

                $groupIsNew = array_search($oneGroup['id'], $idGroupsToSave);

                if ($groupIsNew === false) {
                    continue;
                    //skipping earlier saved groups
                }

                $newGroup = new Groups();
                $newGroup->title = $oneGroup['name'];
                $newGroup->url = \Yii::$app->params['vkRootUrl'] . $oneGroup['screen_name'];
                $newGroup->user_id = $userID;
                $newGroup->id_account = $accountVK->id;
                $newGroup->vk_id = $oneGroup['id'];
                $newGroup->vk_screen_name = $oneGroup['screen_name'];
                $newGroup->vk_photo_200 = $oneGroup['photo_200'];

                if ($newGroup->save(false)) {
                    $countSavedGroups++;
                } else {
                    $countNotSavedGroups++;
                }
            }
        } else {
            return false;
        }

        return 'Всего групп сохранено: ' . $countSavedGroups;
    }

    public function getSavedGroups($accountID, $userID)
    {
        return static::find()
            ->where([
                'id_account' => $accountID,
                'user_id' => $userID,
            ])
            ->all();
    }

    public function getProviderForNewGroups($userGroupsVK)
    {
        return $provider = new ArrayDataProvider([
            'allModels' => $userGroupsVK,
            'sort' => [
                'attributes' => ['title'],
            ],
        ]);
    }

    public function getProviderForSavedGroups($userId, $accountId)
    {
        $query = Groups::find()
        ->where([
            'user_id' => $userId,
            'id_account' => $accountId,
        ]);

        return new ActiveDataProvider([
            'query' => $query,
        ]);

    }
    public static function getGroupsArray($userID){
        $groupsArray =[];
        foreach (Groups::findAll(['user_id' => $userID]) as $one) {
            if (isset($one)){
                $onceID = $one->getAttribute('id');
                $onceTitle = $one->getAttribute('title');
                $groupsArray[] = [
                    'id' => $onceID,
                    'title'=> $onceTitle ];
            } else {
                $groupsArray[] = 'Нет групп';
            }
        }
        $newArray =  ArrayHelper::map($groupsArray, 'id', 'title');

        return $newArray;

    }
}
