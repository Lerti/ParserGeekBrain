<?php

namespace app\models\tables;

use Yii;

/**
 * This is the model class for table "post_schedule".
 *
 * @property int $id
 * @property int $post_final_id
 * @property int $group_id
 * @property int $user_id
 * @property int $account_id
 * @property string $dateTime
 */
class PostSchedule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post_schedule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['post_final_id', 'group_id', 'user_id', 'account_id'], 'integer'],
            [['dateTime'], 'safe'],
            [['group_id','dateTime'], 'required'],
            [['dateTime'], 'string', 'length' => 16],
            ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_final_id' => 'Post Final ID',
            'group_id' => 'Group ID',
            'user_id' => 'User ID',
            'account_id' => 'Account ID',
            'dateTime' => 'Дата и время отправки',
        ];
    }
    public static function getStatus($status)
    {
        switch ($status) {
            case 1:
                return 'Ожидает отправки';
            case 0:
                return 'Отправлено';
            case 100:
                return 'Ошибка отправки';
            default:
                return 'Статус неизвестен';
        }
    }
}
