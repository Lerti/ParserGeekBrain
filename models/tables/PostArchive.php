<?php

namespace app\models\tables;

use Yii;

/**
 * This is the model class for table "post_archive".
 *
 * @property int $id
 * @property string $text
 * @property string $tags
 * @property int $status
 * @property int $size_content
 * @property int $id_user
 * @property string $id_vk
 * @property string $owner_id
 * @property string $date
 * @property int $marked_as_ads
 * @property string $post_type
 * @property string $id_group
 */
class PostArchive extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post_archive';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'status', 'size_content', 'id_user', 'marked_as_ads'], 'integer'],
            [['text'], 'string'],
            [['tags', 'id_vk', 'owner_id', 'date', 'post_type', 'id_group'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Text',
            'tags' => 'Tags',
            'status' => 'Status',
            'size_content' => 'Size Content',
            'id_user' => 'Id User',
            'id_vk' => 'Id Vk',
            'owner_id' => 'Owner ID',
            'date' => 'Date',
            'marked_as_ads' => 'Marked As Ads',
            'post_type' => 'Post Type',
            'id_group' => 'Id Group',
        ];
    }
}
