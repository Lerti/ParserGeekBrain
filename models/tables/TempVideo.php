<?php

namespace app\models\tables;

use Yii;

/**
 * This is the model class for table "temp_video".
 *
 * @property int $id
 * @property int $id_post_intermediate
 * @property string $title
 * @property string $duration
 * @property string $description
 * @property string $track_code
 * @property string $photo_130
 * @property string $photo_320
 * @property string $photo_800
 * @property string $photo_1280
 * @property string $first_frame_130
 * @property string $first_frame_160
 * @property string $first_frame_320
 * @property string $first_frame_720
 * @property string $first_frame_800
 * @property string $first_frame_1024
 * @property string $first_frame_1280
 * @property string $first_frame_4096
 */
class TempVideo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'temp_video';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_post_intermediate'], 'integer'],
            [['description'], 'string'],
            [['title', 'duration', 'track_code', 'photo_130', 'photo_320', 'photo_800', 'photo_1280', 'first_frame_130', 'first_frame_160', 'first_frame_320', 'first_frame_720', 'first_frame_800', 'first_frame_1024', 'first_frame_1280', 'first_frame_4096'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_post_intermediate' => 'Id Post Intermediate',
            'title' => 'Title',
            'duration' => 'Duration',
            'description' => 'Description',
            'track_code' => 'Track Code',
            'photo_130' => 'Photo 130',
            'photo_320' => 'Photo 320',
            'photo_800' => 'Photo 800',
            'photo_1280' => 'Photo 1280',
            'first_frame_130' => 'First Frame 130',
            'first_frame_160' => 'First Frame 160',
            'first_frame_320' => 'First Frame 320',
            'first_frame_720' => 'First Frame 720',
            'first_frame_800' => 'First Frame 800',
            'first_frame_1024' => 'First Frame 1024',
            'first_frame_1280' => 'First Frame 1280',
            'first_frame_4096' => 'First Frame 4096',
        ];
    }

    public function getPostIntermediate ()
    {
        return $this->hasOne(PostIntermediate::className(), ['id' => 'id_post_intermediate']);

    }
}
