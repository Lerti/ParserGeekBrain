<?php

namespace app\models\tables;

use app\models\User;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "applications".
 *
 * @property int $id
 * @property string $name
 * @property int $app_id
 * @property string $secret_key
 * @property string $proxy
 * @property int $network_id
 * @property int $user_id
 * @property int $user
 * @property int $network_accounts
 */
class Applications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'applications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'network_id', 'app_id', 'secret_key'], 'required'],
            [['app_id', 'network_id', 'user_id'], 'integer'],
            [['name', 'proxy'], 'string', 'max' => 255],
            [['secret_key'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название приложения',
            'app_id' => 'ID вашего приложения',
            'secret_key' => 'Защищённый ключ',
            'proxy' => 'Прокси-сервер',
            'network_id' => 'Социальная сеть',
            'user_id' => 'User ID',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function findApplications($userID)
    {
        return static::find()->
        where(['user_id' => $userID])
        ->all();
    }

    public function getProvider($userID)
    {
        $query = static::find()
        ->where(['user_id' => $userID]);

        $provider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'network_id' => SORT_DESC,
                ],
            ],
        ]);
    }

}
