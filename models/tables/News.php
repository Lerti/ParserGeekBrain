<?php

namespace app\models\tables;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $text
 * @property string $header
 * @property string $author
 * @property string $date_create
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['header'], 'string'],
            [['date_create'], 'safe'],
            [['author'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'header' => 'Заголовок',
            'author' => 'Автор',
            'date_create' => 'Дата создания',
            'text' => 'Text',
            'author' => 'Author',
            'date_create' => 'Date Create',
        ];
    }
}
