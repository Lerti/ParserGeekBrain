<?php

namespace app\models\tables;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

/**
 * This is the model class for table "post_intermediate".
 *
 * @property int $id
 * @property string $text
 * @property string $tags
 * @property string $url_group
 * @property string $id_vk
 * @property string $owner_id
 * @property string $date
 * @property int $marked_as_ads
 * @property string $post_type
 * @property string $id_group
 * @property string $photos
 * @property string $temp_video
 * @property string $temp_audio
 * @property string $user_id
 * @property int $likes
 * @property int $comments
 * @property int $reposts
 * @property int $views
 *
 */
class PostIntermediate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post_intermediate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['marked_as_ads'], 'integer'],
            [['tags', 'url_group', 'id_vk', 'owner_id', 'date', 'post_type', 'id_group'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Text',
            'tags' => 'Tags',
            'url_group' => 'Url Group',
            'id_vk' => 'Id Vk',
            'owner_id' => 'Owner ID',
            'date' => 'Date',
            'marked_as_ads' => 'Marked As Ads',
            'post_type' => 'Post Type',
            'id_group' => 'Id Group',
            'likes' => 'likes',
            'comments' => 'comments',
            'reposts' => 'reposts',
            'views' => 'views',
        ];
    }

    public function clearBuffer($userID)
    {
        static::deleteAll(['user_id' => $userID]);
        TempPhoto::deleteAll(['user_id' => $userID]);
        TempVideo::deleteAll(['user_id' => $userID]);
        TempAudio::deleteAll(['user_id' => $userID]);

    }


    public function getTemp_video()
    {
        return $this->hasMany(TempVideo::className(), ['id_post_intermediate' => 'id']);


    }

    public function getTemp_audio()
    {
        return $this->hasMany(TempAudio::className(), ['id_post_intermediate' => 'id']);

    }

    public function getPhotos()
    {
        return $this->hasMany(TempPhoto::class, ['id_post_intermediate' => 'id']);

    }

    /**
     * @return ActiveDataProvider
     */
    public function providerPost()
    {
        $user = Yii::$app->user->identity->getId();

        $query = $this->find()->where(['user_id' => $user]);

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_ASC,
                    ]
                ],
                'pagination' => false,
            ]
        );

        return $dataProvider;
    }
}
