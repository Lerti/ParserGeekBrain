<?php

namespace app\models\tables;

use app\models\VkAuth;
use yii\data\ActiveDataProvider;
use yii\web\User;

/**
 * This is the model class for table "network_accounts".
 *
 * @property int $id
 * @property int $user_id
 * @property string $access_token
 * @property int $id_network
 * @property int $sm_id
 * @property string $account_url
 * @property string $application_id
 * @property string $access_expires_in
 * @property string $user
 * @property string $vk_username
 * @property string $accessTokenExpires
 */
class NetworkAccounts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'network_accounts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'id_network', 'sm_id'], 'integer'],
            [['access_token', 'account_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'access_token' => 'Access Token',
            'id_network' => 'Id Network',
            'sm_id' => 'Sm ID',
            'account_url' => 'Account Url',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);

    }

    public function findAccounts($userID)
    {
        //if it will be allowed for user to have many accounts for one social media, change the search logic

        return static::find()
            ->where(['user_id' => $userID])
            ->one();
    }

    public function getProvider($userID)
    {
        $query = static::find()
            ->where(['user_id' => $userID]);

        return $provider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id_network' => SORT_DESC,
                ],
            ],
        ]);
    }

    public static function getVKAccount($userID)
    {

        return static::find()
            ->where([
                'user_id' => $userID,
                'id_network' => ((new VkAuth())->networkID)
            ])
            ->one();
    }

    public static function displayAccessToken ($userID) {

        $account = static::getVKAccount($userID);
        return $account->access_token;
}
}
