<?php

namespace app\models\tables;

use Yii;

/**
 * This is the model class for table "temp_audio".
 *
 * @property int $id
 * @property int $id_post_intermediate
 * @property string $artist
 * @property string $title
 * @property int $duration
 * @property string $url
 */
class TempAudio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'temp_audio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_post_intermediate', 'duration'], 'integer'],
            [['artist', 'title', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_post_intermediate' => 'Id Post Intermediate',
            'artist' => 'Artist',
            'title' => 'Title',
            'duration' => 'Duration',
            'url' => 'Url',
        ];
    }

    public function getPostIntermediate ()
    {
        return $this->hasOne(PostIntermediate::className(), ['id' => 'id_post_intermediate']);

    }
}
