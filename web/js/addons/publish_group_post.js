/**
 * Переключение групповой и единичной отправки
 * @param view
 * @param requestData
 * @returns {$}
 */
$.fn.inspectionRadioButton = function (view, requestData) {

    let process = {
        checkboxes: $('.publish_checkbox'),
        blockMassPosting: $('.publish_mass_posting_box'),
        formRadioButton: $('.form_toggle'),

        setPostingMethod: function (id) {
            if (this.checkMethodPosting(id)) {
                return this.renderAlone();
            }
            return this.renderMass();
        },

        checkMethodPosting: function (id) {
            if(id === 'fid-1'){
                return true;
            }
            return false;
        },
        renderAlone: function(){
            this.checkboxes.css("display","none");
            this.blockMassPosting.css("display","none");
            return this;
        },
        renderMass: function () {
            this.checkboxes.css("display","block");
            this.blockMassPosting.css("display","flex");
            return this;
        }
    },
        callBackAction = {

            addPropChecked: function (e) {
            if (e.target.nodeName === "INPUT") {
                 process.setPostingMethod(e.target.id);
            }
                return this;
            },

        };

    process.formRadioButton.on('click',callBackAction.addPropChecked);
    return this;
};



$(function()
{
    let view = $(this),
        requestData = {
            _csrf: $('meta[name="csrf-token"]').attr('content'),
        };

    view.inspectionRadioButton(view, requestData);

    return this;
});