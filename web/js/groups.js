$('#save-groups').click(function () {

    let keys = [];
    $.each($("input[name='selection[]']:checked"), function () {
        keys.push($(this).val());
    });

    if (keys.length < 1) {
        alert('Группы не выбраны');
        return;
    }

    //console.log(keys);

    $.ajax({
        method: 'post',
        url: 'groups/save',
        data: {keylist: keys}
    })
        .done(function (msg) {
            if (msg == 'success') {
                window.location.href = '/groups?save=success';
            }
        });
});