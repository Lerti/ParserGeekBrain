/** Здесь собраны активности сайта, различные мелкие скрипты ради которых не имеет смысла создавать фаил

/**
 *
 */
$(window).on('load', function() {
    $('.publish-preview-content').readmore(
        {
            speed: 200,
            maxHeight: 75,
            heightMargin: 16,
            moreLink: '<i class="fa fa-chevron-down publish_arrow_deploy" aria-hidden="true" style="margin-left: 49%; margin-top: 2rem"></i>',
            lessLink: '<i class="fa fa-chevron-up publish_arrow_deploy" aria-hidden="true" style="margin-left: 49%; margin-top: 2rem"></i>'
        });
});

/**
 * checkbox publish
 */
$(document).ready(() => {
    let $scheduleStatusPosts = $('.publish_post_schedule_elem').find('.publish_post_schedule_elem__status');
    $.each($scheduleStatusPosts,function (key,value) {
        let $scheduleStatus = $(value).html();
        switch ($scheduleStatus) {
            case "Отправлено":
                $(value).css({"color":"green"});
                break;
            case "Ошибка отправки":
                $(value).css({"color":"red"});
                break;

        }
    });


    $('.publish_checkbox').click(function(){
        let $idCheckbox = parseInt($(this).attr('id').replace(/\D+/g,""));
        let $idPost ="#post_"+$idCheckbox;
        if ($(this).is(':checked')){
            $($($idPost).parent()).css({"box-shadow":"1px 3px 5px 2px #1BAD2D"});
        } else {
            $($($idPost).parent()).css({"box-shadow":"1px 3px 5px 2px #ADADAD"});
        }
    });

    $(".for-posts").on('click','.publish-preview-image', function(e){
        let $idPost = parseInt($(e.target).parents('.publish-preview-post').attr('id').replace(/\D+/g,""));
        let $idCheckbox ="#checkbox_"+$idPost;
        $($($idCheckbox)).trigger('click');
        return false;
    });


        $(".publish-preview-post").on('click','.publish_edit_button', function(e){
            let $widthImg = $(".publish_post_edit__image");
         $($widthImg).css({"max-width" : "23%"});
        let $idPost = parseInt($(e.target).parents('.publish-preview-post').attr('id').replace(/\D+/g,""));
        let $sub = $('#edit_post_'+$idPost).find('.publish_post_edit__image');
        switch ($sub.length) {
            case 1:
                $($widthImg).css({"max-width" : "98%"});
                break;
            case 2:
                $($widthImg).css({"max-width" : "48%"});
                break;
            case 3:
                $($widthImg).css({"max-width" : "30%"});
                break;
            case 0:
                $($widthImg).css({"max-width" : "98%"});
                break;
        }

    });



    let $count = $('#checkbox_count');
    let $checkboxes = $('.publish_checkbox');
    $('#checkbox_count_max').html($checkboxes.length);
    $count.html('0');
    $checkboxes.click(function () {
        let $newCount = $('.publish_checkbox:checked').length;
        $count.html($newCount);
    });

    /**
     * Button check all/non check all page publish
     */
    $('.all_check').click(function () {
        if (this.id === 'all_check_button') {
            $(this).attr('id','all_non_check_button');
            $(this).html('Снять выделение');
            $($checkboxes).prop('checked', true);
            $($('.publish-preview-post').parent()).css({"box-shadow":"1px 3px 5px 2px #1BAD2D"});

        } else {
            $(this).attr('id','all_check_button');
            $(this).html('Выбрать все');
            $($checkboxes).prop('checked', false);
            $($('.publish-preview-post').parent()).css({"box-shadow":"1px 3px 5px 2px #ADADAD"});

        }
        let $newCount = $('.publish_checkbox:checked').length;
        $count.html($newCount);
    });

    $('#savePosts').click(function () {

        let keys = [];
        let $idCheckbox;
        let $id_group = $('#publish_id_group').val();
        let $interval = $('#publish_interval').val();

        $('.publish_checkbox:checked').each(function() {
            $idCheckbox = parseInt($(this).attr('id').replace(/\D+/g,""));

            keys.push($idCheckbox);
        });


        //console.log($id_group);
        //console.log(keys);

        if(keys.length >0) {
            $.ajax({
                type: "POST",
                url: "publish/pack-send",
                data: {
                    keylist: keys,
                    id_group: $id_group,
                    interval: $interval
                },
                success: function (response) {
                },
                error: function () {
                    console.log('Что-пошло не так при передаче данных из intermediate.')
                },
            });
        }
    });

});



