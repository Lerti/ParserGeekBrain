/**
 * choose avatar
 */
$(document).ready(() => {
    $('.checkbox_avatar').click(function(){
        let $div = $(this).parents('.checkbox');
        if ($(this).is(':checked')){
            $(".checkbox_avatar").prop('checked', false);
            $('.checkbox').css({"box-shadow":"1px 3px 5px 2px #ADADAD"});

            $(this).prop('checked', true);
            $($div).css({"box-shadow":"1px 3px 5px 2px #1BAD2D"});

            let $selectedAvatarName = 'avatar' + $(this).attr('value') + '.jpg';
            $('#selectedAvatarName').text($selectedAvatarName);
        } else {
            $($div).css({"box-shadow":"1px 3px 5px 2px #ADADAD"});
            $('#selectedAvatarName').text('Файл не выбран.');
        }
    });
});