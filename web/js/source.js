$(window).on('load', function () {
    $('.intermediate_text').readmore(
        {
            speed: 200,
            maxHeight: 200,
            heightMargin: 16,
            moreLink: '<i class="fa fa-chevron-down intermediate_arrow_deploy" aria-hidden="true" style="margin-left: 49%; margin-top: 2rem"></i>',
            lessLink: '<i class="fa fa-chevron-up intermediate_arrow_deploy" aria-hidden="true" style="margin-left: 49%; margin-top: 2rem"></i>'
        });
});

$(document).ready(function () {

    /**
     * show hidden buttons
     * */
    /*$('[name="selection[]"]').change(function () {

    $('#savePosts').removeClass('hidden');
}*/
    //полетело при изменении GridView на ListView

    $('#savePosts').click(function () {

        let keys = [];
        let $idCheckbox;

        $('#posts_id :checked').each(function () {
            $idCheckbox = parseInt($(this).attr('id').replace(/\D+/g, ""));

            keys.push($idCheckbox);
        });

        //yiiGridView('getSelectedRows');

        console.log(keys);

        if (keys.length > 0) {
            $.ajax({
                type: "POST",
                url: "source/keys",
                data: {
                    keylist: keys
                },
                success: function (response) {

                    switch (response) {
                        case 'error1':
                            alert('Посты не сохранены. Возможно, они уже есть в ваших сохраненных публикациях');
                            window.location.href = '/source';
                            break;
                        case 'error2':
                            alert('Запрос не обработан на сервере. Попробуйте еще раз.');
                            window.location.href = '/source';
                            break;
                        default:
                            alert('Сохранено постов: ' + response);
                            window.location.href = '/publish';
                    }
                },
                error: function () {
                    // console.log('Что-пошло не так при передаче данных из intermediate.')
                },
            });
        }
    });

    /**
     * add box-shadow to selected post
     * */
    $('.intermediate_checkbox').click(function () {
        let $idCheckbox = parseInt($(this).attr('id').replace(/\D+/g, ""));
        let $idPost = "#post_" + $idCheckbox;
        if ($(this).is(':checked')) {
            $($($idPost).parent()).css({"box-shadow": "1px 3px 5px 2px #1BAD2D"});
        } else {
            $($($idPost).parent()).css({"box-shadow": "1px 3px 5px 2px #ADADAD"});
        }
    });

    /**
     * select all posts
     * */
    $('#select-all-button').click(function () {
        $(".intermediate_checkbox").prop('checked', true);
        $($(".intermediate_preview_post").parent()).css({"box-shadow": "1px 3px 5px 2px #1BAD2D"});
    });
});
