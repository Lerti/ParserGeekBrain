<?php
/**
 * 05.09.2019
 * 21:57
 */

namespace app\assets;


use yii\web\AssetBundle;

class AccountAsset extends AssetBundle
{
    /*public $basePath = '@webroot';
    public $baseUrl = '@web';*/
    public $css = [
        'css/_account.css',
    ];
    public $js = [
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];


}