<?php
/**
 * 05.09.2019
 * 21:57
 */

namespace app\assets;


use yii\web\AssetBundle;

class GroupsAsset extends AssetBundle
{

    public $css = [

    ];
    public $js = [
        'js/groups.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];


}