<?php

namespace app\assets;


use yii\web\AssetBundle;

class PublishAsset extends AssetBundle
{
    /*public $basePath = '@webroot';
    public $baseUrl = '@web';*/
    public $css = [

    ];
    public $js = [
        'js/publish.js',
        'js/addons/publish_group_post.js'

    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];


}