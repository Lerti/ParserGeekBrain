<?php
/**
 * @var $model \app\models\Source
 *
*/

use yii\bootstrap\Modal;
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Html;
use \yii\helpers\Url;

?>
<div>
    <div class = "icon-right-intermediate icon-up-intermediate">
        <i class = "fa fa-calendar" aria-hidden = "true"></i>
        <span><?= date("F j, Y",$model->date);?></span>
    </div>
    <div class = "intermediate_preview_post" id = "post_<?= $model->id?>">
        <div class = "intermediate_post_header">
            <div>
                <label style = "padding-bottom: 10px" title = "Выбрать пост">
                    <input type = "checkbox" id = "checkbox_<?= $model->id?>" name = "intermediate_checkbox"
                        class = "option-input checkbox intermediate_checkbox"/>
                </label>
            </div>
        </div>
        <div class = "publish-preview-content"><?= $model->url_group?> </div>

            <?php if(strlen($model->text) === 0):?>
                <div class = 'intermediate-text-container'>
                    <div class = "publish-preview-content intermediate_text">Нет текста</div>
                </div>
            <? else:?>
                <div class = 'intermediate-text-container'>
                    <div class = "publish-preview-content intermediate_text"><?= $model->text?></div>
                </div>
            <? endif;?>

            <?php if(isset($photo)):?>
                <div class = "intermediate-preview-image">
                    <img src = "<?= $photo?>" alt = "VK_Photo">
                </div>
            <? else:?>
                <div class = "intermediate-preview-image">
                    <img src = "/img/no_photo.jpg" alt = "No Image">
                </div>
            <? endif;?>
    </div>
    <div>
        <div class = "icon-left-intermediate">
            <i class = "fa fa-thumbs-o-up" aria-hidden = "true"></i>
            <span><?= number_format($model->likes, 0, '.', ' ') ?></span>
            <i class = "fa fa-comments-o" aria-hidden = "true"></i>
            <span><?= number_format($model->comments, 0, '.', ' ') ?></span>
            <i class = "fa fa-share" aria-hidden = "true"></i>
            <span><?= number_format($model->reposts, 0, '.', ' ') ?></span>
        </div>
        <div class = "icon-right-intermediate">
            <i class = "fa fa-eye" aria-hidden = "true"></i>
            <span><?= number_format($model->views, 0, '.', ' ') ?></span>
        </div>
        </div>

</div>
