<?php
/**
 * Created by PhpStorm.
 * User: NEMOY
 * Date: 05.09.2019
 * Time: 20:53
 */

namespace app\widgets;


use app\models\IntermediateFinal;
use app\models\tables\MediaContentPhoto;
use app\models\Source;
use app\models\tables\PostIntermediate;
use app\models\tables\TempPhoto;
use yii\base\Widget;
use yii\helpers\Html;

class IntermediatePreview extends Widget
{
    public $model;
    public $pages;

    public function run(){
        if(is_a($this->model, PostIntermediate::class)){
            foreach ($this->getPhotoArray($this->model->id) as $one){
                if ($one) {
                    $url = $one;
                }
            }

            return $this->render('intermediate_preview', [
                'model' => $this->model,
                'photo' => $url,
            ]);
        }
    }

    private function getPhotoArray($id_item){

        $photoArray = [];
        foreach (TempPhoto::findAll(['id_post_intermediate' => $id_item]) as $one) {

            $once = $one->getAttributes($one->photoSizes);
            foreach ($once as $item) {
                if ($item) {
                    $url = $item;
                }
            }
            $photoArray[] = $url;
        }

       return  $photoArray;
    }

}