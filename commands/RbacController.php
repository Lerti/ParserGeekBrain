<?php


namespace app\commands;


use yii\console\Controller;

class RbacController extends Controller
{

    public function actionIndex()
    {
        $am = \Yii::$app->authManager;

        $admin = $am->createRole("admin");
        $moder = $am->createRole("moder");

        $am->add($admin);
        $am->add($moder);

        $permisionNewsCreate = $am->createPermission("NewsCreate");
        $permisionNewsUpdate = $am->createPermission("NewsUpdate");
        $permisionNewsDelete = $am->createPermission("NewsDelete");
        $permisionEditRoles = $am->createPermission("EditRoles");

        $am->add($permisionNewsCreate);
        $am->add($permisionNewsUpdate);
        $am->add($permisionNewsDelete);
        $am->add($permisionEditRoles);

        $am->addChild($admin,$permisionNewsCreate);
        $am->addChild($admin,$permisionNewsUpdate);
        $am->addChild($admin,$permisionNewsDelete);
        $am->addChild($admin,$permisionEditRoles);

        $am->addChild($moder,$permisionNewsCreate);
        $am->addChild($moder,$permisionNewsUpdate);


        $am->assign($admin,1);
        $am->assign($moder,2);

    }

}