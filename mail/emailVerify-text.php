<?
$resetLink = Yii::$app->urlManager->createAbsoluteUrl([
'user/verification-email', 'token' => $user->verification_token]);
?>

Здравствуйте, <?= $user->username ?>!

Вы успешно зарегистрировались на сайте

Ссылка для подтверждения регистрации: .

Если вы не понимаете, о чем идет речь, проигнорируйте данное письмо.

С уважением,
    Мы.
