<?
/**
 * Displays homepage.
 *
 * @var $user app/models/User
 * @var verification_token string
 */

use yii\helpers\Html;

$resetLink = Yii::$app->urlManager->createAbsoluteUrl([
    'user/verification-email', 'token' => $user->verification_token]);
$siteLink = Yii::$app->urlManager->createAbsoluteUrl('site/index');
?>

<div class="email-verification">
    <p>Здравствуйте, <?= Html::encode($user->username) ?>!</p>
    <p>Вы успешно зарегистрировались на сайте <?= Html::a(Html::encode('parsernash.ru'), $siteLink) ?>.
    <p>Ссылка для подтверждения регистрации:
        <?= Html::a(Html::encode('Подтвердить.'), $resetLink) ?></p>
    <p>Если вы не понимаете, о чем идет речь, проигнорируйте данное письмо. </p>
    <br/>
    <p>
        С уважением,
        Мы.
    </p>
</div>