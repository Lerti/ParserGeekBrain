<?
$resetLink = Yii::$app->urlManager->createAbsoluteUrl([
'user/verification-email', 'token' => $user->verification_token]);
?>

Здравствуйте, <?= $user->username ?>!

Вы запросили восстановление пароля на сайте

Ссылка для завершения операции восстановления пароля: .

Если вы не запрашивали восстановление пароля, проигнорируйте данное письмо.

С уважением,
    Мы.
