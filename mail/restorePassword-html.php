<?
/**
 * Displays homepage.
 *
 * @var $user \app\models\User
 * @var $password_reset_token string
 */

use yii\helpers\Html;

$resetLink = Yii::$app->urlManager->createAbsoluteUrl([
    'user/verification-password-token', 'token' => $user->password_reset_token]);
$siteLink = Yii::$app->urlManager->createAbsoluteUrl('site/index');
?>

<div class="email-verification">
    <p>Здравствуйте, <?= Html::encode($user->username) ?>!</p>
    <p>Вы запросили восстановление пароля на сайте <?= Html::a(Html::encode('parsernash.ru'), $siteLink) ?>.
    <p>Ссылка для завершения операции восстановления пароля:
        <?= Html::a(Html::encode('Восстановить.'), $resetLink) ?></p>
    <p>Если вы не запрашивали восстановление пароля, проигнорируйте данное письмо. </p>
    <br/>
    <p>
        С уважением,
        Мы.
    </p>
</div>