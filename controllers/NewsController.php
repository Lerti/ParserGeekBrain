<?php
/**
 * Created by PhpStorm.
 * User: Dmytry Nebolsin
 * Date: 03.09.2019
 * Time: 17:24
 */

namespace app\controllers;


use app\models\NewsAddForm;
use app\models\tables\News;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;

class NewsController extends Controller
{

    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'delete', 'save', 'update'],
                'rules' => [
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['NewsCreate'],
                    ],
                    [
                        'actions' => ['update', 'save'],
                        'allow' => true,
                        'roles' => ['NewsUpdate'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['NewsDelete'],
                    ],
                ],
                'denyCallback' => function(){
                    echo 'Доступ запрещен';
                    exit();
                },
            ],
        ];
    }


public function actionIndex(){
    $query = News::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);
    return $this->render('index', [
        'dataProvider' => $dataProvider,
    ]);
}
    public function actionView($id){
        return $this->render('view', [
            'model' => News::findOne($id)
        ]);
    }
    public function actionUpdate($id){
        return $this->render('update', [
            'model' => News::findOne($id)
        ]);
    }

    public function actionSave($id){
        if ($model = News::findOne($id)) {
            $model->load(\Yii::$app->request->post());
            $model->save();
            \Yii::$app->session->setFlash('success', "Измения сохранены");
        }  else {
            \Yii::$app->session->setFlash('error', "Не удалось сохранить изменения");
        }
        $this->redirect(\Yii::$app->request->referrer);
    }

    public function actionCreate(){
        $model = new News();
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['news/view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);

    }
    public function actionDelete($id)
    {
        News::findOne($id)->delete();

        return $this->redirect(['index']);
    }
}