<?php
/**
 * 26.09.2019
 * 22:03
 */

namespace app\controllers;

use app\models\tables\Groups;
use app\models\tables\NetworkAccounts;
use app\models\VkAuth;
use yii\web\Controller;

class GroupsController extends Controller
{

    public function actionIndex()
    {
        $userID = \Yii::$app->getUser()->getId();
        $account = NetworkAccounts::getVKAccount($userID);

        if (\Yii::$app->request->get('save') == 'success') {

            //todo display success msg to user
        }

        if (isset($account)) {
            $userGroupsVK = (new VkAuth())->getGroups($account);
            $savedGroups = (new Groups())->getSavedGroups($account->id, $userID);
            $providerNewGroups = (new Groups())->getProviderForNewGroups($userGroupsVK);

        } else {
            $userGroupsVK = null;
            $savedGroups = null;
        };

        if (!empty($savedGroups)) {

            $providerSavedGroups = (new Groups())->getProviderForSavedGroups($userID, $account->id);

            return $this->render('_userGroups', [
                'provider' => $providerSavedGroups,
                'providerNewGroups' => $providerNewGroups,

            ]);

        }

        if (isset($account)) {

            return $this->render('_groupChoice', [
                'providerNewGroups' => $providerNewGroups,
            ]);
        } else {

            return $this->render('_groupWarning');
        }
    }

    public function actionSave()
    {
        if (\Yii::$app->request->isAjax) {

            $userID = \Yii::$app->getUser()->getId();
            $keyList = \Yii::$app->request->post('keylist');

            return (new Groups())->saveUserGroups($keyList, $userID)
                ? 'success' : 'error';
        }

        return 'error';
    }

    public function actionDelete($id)
    {
        $userID = \Yii::$app->getUser()->getId();

        Groups::deleteAll([
            'id' => $id,
            'user_id' => $userID,
        ]);

        $this->redirect('/groups');
    }

}