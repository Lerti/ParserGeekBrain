<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\LoginForm;
use yii\web\Response;
use app\models\SignupForm;
use yii\web\UploadedFile;
use app\models\User;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;

class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
           /* 'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $title = 'Вход';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        $model->password = '';
        return $this->render('login', [
            'title' => $title,
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
      public function actionLogout()
      {
          Yii::$app->user->logout();

          return $this->goHome();
      }

    /**
     * Displays create page.
     *
     * @return string
     */
    public function actionSignup()
    {
        Yii::$app->name = 'Регистрация';
        $buttonName = 'Зарегистрироваться';

        $model = new SignupForm();

        if(isset($_POST['SignupForm'])){
            $model->attributes = $_POST['SignupForm'];
            $model->selectedAvatar = $_POST['SignupForm']['selectedAvatar'][0];
            if($model->signup()){
                Yii::$app->session->setFlash('success',
                    'Спасибо за регистрацию. На указанный email было отправлено письмо для её завершения.');
                return $this->goHome();
            }
        }

        $title = 'Регистрация';
        $registration = true;
        return $this->render('signup', [
            'title' => $title,
            'model' => $model,
            'buttonName' => $buttonName,
            'registration' => $registration,
        ]);
    }

    /**
     * Profile page.
     * @return Response|string
     */
    public function actionProfile()
    {
        Yii::$app->name = 'Профиль';
        $user = new User;
        $id = Yii::$app->user->identity->id;
        $model = $user->findIdentity($id);
        $title = 'Профиль';
        return $this->render('profile', [
            'title' => $title,
            'model' => $model,
        ]);
    }


    /**
     * Delete page.
     * @return string
     */
    public function actionDelete()
    {
        //$user = new User;
        $title = 'Удалить';
        $id = Yii::$app->user->identity->id;

        return $this->render('delete_user', [
            'title' => $title,
            'user_id' => $id,
        ]);
    }

    /**
    * Delete user action.
    * @return string
    */
    public function actionDelete_user()
    {
        $user = new User;
        $id = Yii::$app->user->identity->id;

        if($user->deleteUser($id)) {
            Yii::$app->session->setFlash('success', 'Ваш аккаунт удален.');
            return $this->goHome();
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'profile' page.
     * @return mixed
     */
    public function actionUpdate_information()
    {
        Yii::$app->name = 'Профиль';
        $title = 'Изменение профиля';
        $buttonName = 'Сохранить изменения';

        $user = new User;
        $id = Yii::$app->user->identity->id;
        $model = $user->findIdentity($id);


        if(isset($_POST['User'])) {

            $answer = $model->selectChange($_POST['User']);

            if($answer === 'success') {
                $model->updateUser($_POST['User'], $_FILES['User']['name']['avatar']);
                Yii::$app->session->setFlash('success', 'Изменение ваших данных прошло успешно.');

                return $this->redirect(['profile',
                'id' => $model->id,
                'model' => $model,
            ]);
            } else {
                $model = $answer;
            }
        }

        $registration = false;
        return $this->render('signup', [
            'title' => $title,
            'model' => $model,
            'buttonName' => $buttonName,
            'registration' => $registration,
        ]);
    }

    /**
     * Change password action.
     * If update is successful, the browser will be redirected to the 'profile' page.
     * @return string
     */
    public function actionUpdate_password() {

        $model = new User;
        $id = Yii::$app->user->identity->id;
        $model->scenario = User::SCENARIO_UPDATE_PASSWORD;
        if(isset($_POST['User'])) {
            $model->setAttributes($_POST['User']);
            if($model->validate()) {
                $newUser = $model->updatePassword($id);

                Yii::$app->session->setFlash('success', 'Изменение вашего пароля прошло успешно.');

                return $this->redirect(['profile',
                    'id' => $newUser->id,
                    'model' => $newUser,]);
            }
        }

        return $this->render('password', [
            'model' => $model,
        ]);
    }

    /**
     * Validate email
     *
     * @param string $token
     * @return mixed
     */
    public function actionVerificationEmail($token)
    {
        $user = User::findByEmailVerificationToken($token);

        if(is_null($user)) {
            Yii::$app->session->setFlash('error', 'Пользователь с таким email не существует. ' .
            'Попробуйте зарегистрироваться снова или обратитесь в службу поддержки сайта.');
            return $this->goHome();
        }

        if($user->status === User::STATUS_DELETED) {
            Yii::$app->session->setFlash('error', 'Пользователь с таким email был удален. ' .
                'Попробуйте зарегистрироваться снова под другим email или обратитесь в службу поддержки сайта.');
            return $this->goHome();
        } else if($user->status === User::STATUS_ACTIVE) {
            Yii::$app->session->setFlash('success', 'Ваш email уже подтвержден.');
            return $this->goHome();
        }

        if($user->isEmailVerificationTokenValid($token)) {
            $user->validateEmail();
            Yii::$app->session->setFlash('success', 'Вы подтвердили свой email и можете пользоваться данным сайтом.');
            return $this->goHome();
        } else {
            $title = 'Регистрация';
            Yii::$app->session->setFlash('error', 'Срок действия данной ссылки подтверждения email истек.');
            return $this->render('send_email', [
                'title' => $title,
                'user' => $user,
            ]);
        }
      }

    /**
     * Send email with verification_token again
     *
     * @param $id int
     * @return mixed
     */
      public function actionSendEmail($id) {

          $user = User::findById($id);
          if(SignupForm::sendEmail($user)) {
              Yii::$app->session->setFlash('success',
                  'Спасибо за регистрацию. На указанный email было отправлено письмо для её завершения.');
              return $this->goHome();
          }
          Yii::$app->session->setFlash('error',
              'Письмо не отправилось. Попробуйете еще раз.');
          return $this->goHome();

      }

    /**
     * If user forgot password, he can restore password
     * For it he should enter his email
     * @return mixed
     */
      public function actionRestorePassword() {

          $title = 'Восстановить';
          $this->layout = ('@app/views/layouts/main-login');
          $message = '';

          if (!Yii::$app->user->isGuest) {
              return $this->goHome();
          }

          $model = new User();
          $model->scenario = User::SCENARIO_RESTORE_PASSWORD;

          if(isset($_POST['User'])) {
              $user = User::findByEmail($_POST['User']['email']);
              if(is_null($user)) {
                  $message = 'Пользователя с таким email не существует.';
                  $model->email = $_POST['User']['email'];
              } else {
                  if($user->status === User::STATUS_DELETED) {
                      $message = 'Пользователь с таким email был удален.';
                      $model->email = $_POST['User']['email'];
                  } else if($user->status === User::STATUS_NOT_ACTIVE) {
                      $message = 'Данный email не подтвержден.';
                      $model->email = $_POST['User']['email'];
                  } else if($user->status === User::STATUS_ACTIVE) {
                      if(User::sendPasswordEmail($user)) {
                          return $this->render('send_token', [
                              'title' => $title,
                          ]);
                      }
                      $message = 'На указанный email уже было отправлено письмо со ссылкой для восстановления пароля.';
                  }
              }
          }
          return $this->render('new_password', [
              'title' => $title,
              'model' => $model,
              'message' => $message,
          ]);
      }

    /**
     * Validate password_reset_token
     *
     * @param string $token
     * @return mixed
     */
    public function actionVerificationPasswordToken($token)
    {
        $user = User::findByPasswordResetToken($token);

        if(is_null($user)) {
            Yii::$app->session->setFlash('error', 'Пользователь с таким email не существует. ' .
                'Попробуйте зарегистрироваться снова или обратитесь в службу поддержки сайта.');
            return $this->goHome();
        }

        $title = 'Восстановить';
        if($user->isPasswordResetTokenValid($token)) {
            $user->scenario = User::SCENARIO_GENERATE_PASSWORD;

            if(isset($_POST['User'])) {
                $user->setPassword($_POST['User']['password']);
                if($user->save()) {
                    Yii::$app->session->setFlash('success', 'Изменение пароля прошло успешно.');
                    return $this->goHome();
                } else {
                    Yii::$app->session->setFlash('error', 'Изменение пароля окончилось крахом. ' .
                        'Попробуйте еще раз или обратитесь в службу поддержки сайта.');
                }
            }

            return $this->render('new_password_generate', [
                'title' => $title,
                'model' => $user,
            ]);
        } else {
            Yii::$app->session->setFlash('error', 'Срок действия данной ссылки для восстановления пароля истек.');
            return $this->render('send_password_token', [
                'title' => $title,
            ]);
        }
    }
}