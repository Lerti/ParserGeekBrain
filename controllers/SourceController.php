<?php
/**
 * 29.08.2019
 * 20:28
 */

namespace app\controllers;


use app\models\IntermediateFinal;
use app\models\ReplacePosts;
use app\models\Source;
use app\models\tables\NetworkAccounts;
use app\models\tables\PostIntermediate;
use yii\web\Controller;

class SourceController extends Controller
{
    public function actionIndex()
    {
        $user = \Yii::$app->getUser()->getIdentity();
        $userID = $user->getId();
        $accountVK = NetworkAccounts::getVKAccount($userID);

        $source = new Source();
        $post = \Yii::$app->request->post();
        $message = 0;
        $checkboxesText = $source->filterCheckboxes;

        if (isset($post['groupUrl'])) {

            (new PostIntermediate())->clearBuffer($userID);

            $groupUrl = $post['groupUrl'];
            $postsNumber = $post['selector'];
            $filterOptions = $post['filter-posts'];

            $message = $source->getPosts(
                $groupUrl, $filterOptions, $userID, $accountVK, $postsNumber);
        }

        /** @var  $selectors array of numbers from 10 to 100 */
        $selectors = $source->generatePostsNumber();

        /** @var $selectAccount  array of objects with VK accounts of User */
        $selectAccount = $source->displaySelectAccount($user);

        $dataProvider = (new PostIntermediate())->providerPost();

        return $this->render('posts.php', [
            'selectors' => $selectors,
            'message' => $message,
            'dataProvider' => $dataProvider,
            'checkboxesText' => $checkboxesText,
            'selectAccount' => $selectAccount,
            'account' => $accountVK,
        ]);
    }

    public function actionKeys()
    {

        if (\Yii::$app->request->isAjax) {

            $keyList = \Yii::$app->request->post('keylist');
            $postsSaved = (new ReplacePosts())->start($keyList);

            return $postsSaved ?: 'error1';
        }
        return 'error2';
    }

}