<?php
/**
 * 23.09.2019
 * 21:37
 */

namespace app\controllers;

use app\models\tables\Groups;
use Codeception\Platform\Group;
use VK\Client\VKApiClient;
use app\models\tables\NetworkAccounts;
use app\models\VkAuth;
use yii\web\Controller;
use app\models\tables\Applications;

class AccountsController extends Controller
{

    public function actionIndex()
    {
        $userID = \Yii::$app->getUser()->getId();
        $app = new Applications();
        $acc = new NetworkAccounts();

        $userApplications = $app->findApplications($userID);
        $userAccounts = $acc->findAccounts($userID);

        $provider = $acc->getProvider($userID);

        return $this->render('index', [
            'provider' => $provider,
            'userApplications' => $userApplications,
            'userAccounts' => $userAccounts,
        ]);
    }
    public function actionGetCode()
    {
        $userID = \Yii::$app->getUser()->getId();
        $vkAuth = new VkAuth();

        $app = Applications::find()
            ->where(['user_id' => $userID, 'network_id' => $vkAuth->networkID])
            ->one();

        $this->redirect($vkAuth->linkForGetCode($app->app_id));
    }

    public function actionParseCode()
    {
        $codeFromUser = \Yii::$app->request->post()['code-input'];

        $validateWord = '/^https:\/\/oauth.vk.com\/blank.html#code=/';
        //проверяет, начинается ли $codeFromUser на https://oauth.vk.com/blank.html#code=

        if(strlen($codeFromUser)<= 37 || strlen($codeFromUser) >= 255) {
            echo $this->render('_modal', [
                'errorCodeFromUser' => 'length',
            ]);
        } else {
            if (preg_match($validateWord, $codeFromUser)) {

                $code = (new VkAuth())->parseCode($codeFromUser);
                $app = Applications::find()
                    ->where(['user_id' => \Yii::$app->getUser()->getId()])
                    ->one();
                $vkAuth = new VkAuth();
                $vkAuth->getAccessToken($code, $app);

            } else {
                echo $this->render('_modal', [
                    'errorCodeFromUser' => 'value',
                ]);
            }
        }
        /*$code = (new VkAuth())->parseCode($codeFromUser);
        $app = Applications::find()
            ->where(['user_id' => \Yii::$app->getUser()->getId()])
            ->one();

        $vkAuth = new VkAuth();
        $vkAuth->getAccessToken($code, $app);*/
    }

    public function actionDelete($id)
    {
        $userID = \Yii::$app->getUser()->getId();

        NetworkAccounts::deleteAll([
            'id' => $id,
            'user_id' => $userID,
        ]);

        Groups::deleteAll([
            'user_id' => $userID,
            'id_account' => $id
        ]);

        $this->redirect('/accounts');

    }

   /* public function actionVk()
    {

        $vk = new VKApiClient();
        $response = $vk->wall()->post('0dd83088b99ec2c9ab6f7998d08e6f60305b9043238905e2bf0a181228c29060bd3674f33fe1314346ce0', array(
            'message' => 'hi',
            //'owner_id' => '559492692',
            'owner_id' => '-186080195',
        ));

    }*/

}