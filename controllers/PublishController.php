<?php
/**
 * Created by PhpStorm.
 * User: NEMOY
 * Date: 03.09.2019
 * Time: 17:35
 */

namespace app\controllers;


use app\models\ReplacePosts;
use app\models\tables\Groups;
use app\models\tables\MediaContentAudio;
use app\models\tables\MediaContentPhoto;
use app\models\tables\MediaContentVideo;
use app\models\tables\PostFinal;
use app\models\tables\PostSchedule;
use app\models\tables\TempPhoto;
use app\models\VkAuth;
use yii\web\Controller;

class PublishController extends Controller
{
    public function actionIndex()
    {
        $dataProvider = (new PostFinal())->providerFinalPosts();
        $addedPosts = ReplacePosts::$postsSaved;
        $userID = \Yii::$app->user->identity->getId();

        return $this->render('index.php', [
            'dataProvider' => $dataProvider,
            'addedPosts' => $addedPosts,
            'groups' => Groups::getGroupsArray($userID),
            'user_id' => $userID,
        ]);
    }

    /**
     * Delete posts
     * @return string
     */
    public function actionDelete()
    {
        $user = \Yii::$app->user->identity->getId();
        if (\Yii::$app->request->isAjax) {
            foreach (PostFinal::findAll(['id_user' => $user]) as $one) {
                TempPhoto::deleteAll(['id_post_intermediate' => $one->id]);
            }
            PostFinal::deleteAll(['id_user' => $user]);

            return 'Все посты удалены';
        }
    }

    /**
     * Edit Post
     * @param $id
     * @return \yii\web\Response
     */
    public function actionEditPost($id)
    {
        $model = PostFinal::findOne($id);
        $model->load(\Yii::$app->request->post());
        $model->save();

        return $this->redirect('/publish');

    }

    /**
     * Create element Schedule
     * @param $id
     * @param $user_id
     * @param $status
     * @return \yii\web\Response
     */
    public function actionCreateSchedule($id, $user_id, $status)
    {
        $dateTime = date('Y-m-d H:i');
        $model = new PostSchedule();
        $model->post_final_id = $id;
        $model->user_id = $user_id;
        $model->status = $status;
        $model->load(\Yii::$app->request->post());

        VkAuth::postImmediatelyVK($user_id, $model->group_id, $model->post_final_id);

        if ($model->dateTime === NULL) {
            $model->dateTime = $dateTime;
        }

        if ($model->validate()) {
            $model->save();
        }

        $group = Groups::find()
            ->where(['id' => $model->group_id])
            ->one();

        $message = 'Пост опубликован в сообществе ' . $group->title;
        \Yii::$app->getSession()->setFlash('success', $message);
        return $this->redirect('/publish');
    }

    /**
     * Copy post
     * @param $id
     * @return \yii\web\Response
     */
    public function actionCopyPost($id)
    {
        $model = new PostFinal();
        $model->attributes = PostFinal::findOne($id)->attributes;
        $model->id = null;
        $model->save();
        foreach (MediaContentPhoto::findAll(['id_post_final' => $id]) as $one) {
            $modelPhoto = new MediaContentPhoto();
            $modelPhoto->attributes = $one->attributes;
            $modelPhoto->id = null;
            $modelPhoto->id_post_final = $model->id;
            $modelPhoto->save();
        }
        return $this->redirect('/publish');
    }

    /**
     * Delete one post
     * @param $id
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeletePost($id)
    {
        $model = PostFinal::findOne($id);
        $model->delete();
        PostSchedule::deleteAll(['post_final_id' => $id]);
        MediaContentPhoto::deleteAll(['id_post_final' => $id]);
        MediaContentAudio::deleteAll(['id_post_final' => $id]);
        MediaContentVideo::deleteAll(['id_post_final' => $id]);

        return $this->redirect('/publish');
    }

    /**
     * Delete one Schedule
     * @param $id
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteSchedule($id)
    {
        $model = PostSchedule::findOne($id);
        $model->delete();

        return $this->redirect('/publish');
    }

    /**
     *
     */
    public function actionPackSend()
    {
        $group_id = \Yii::$app->request->post('id_group');
        $user_id = \Yii::$app->user->identity->getId();
        $interval = \Yii::$app->request->post('interval');

        if (\Yii::$app->request->isAjax) {

            foreach (\Yii::$app->request->post('keylist') as $onePost){
                $post = $onePost;
                //VkAuth::postImmediatelyVK($user_id, $group_id , $onePost);
            }

        }

        $message = 'Опубликовано '. count(\Yii::$app->request->post('keylist')) .' постов в '.$group_id.' группу с интервалом '.$interval.' секунд.';
        \Yii::$app->getSession()->setFlash('success', $message);
        return $this->redirect('/publish');

    }
}