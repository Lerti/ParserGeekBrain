<?php

namespace app\controllers;

use app\models\filters\AppSearch;
use app\models\tables\Applications;
use app\models\tables\Network;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class ApplicationsController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $userID = Yii::$app->getUser()->getId();
        $model = new Applications();

        /*one user = one application, data is rewriting in existing application*/
        $model = Applications::find()
            ->where(['user_id' => $userID])
            ->one()
            ?: $model = new Applications();

        $searchModel = new AppSearch();
        $dataProvider = $searchModel->search($userID);

        $network = Network::find()->all();
        $networkList = ArrayHelper::map($network, 'id', 'name');

        if ($model->load(\Yii::$app->request->post())) {

            $model->user_id = $userID;

            if ($model->validate()) {
                $model->save();
                return $this->redirect(['/applications']);
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'networkList' => $networkList,
        ]);

    }

    public function actionRemove()
    {
        if (\Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            $model = Applications::find()->where(['id' => $id])->one();
            $model->delete();
            return json_encode(['idDelete' => $id]);
        }
    }

    public function actionInstruction()
    {
        return $this->render('instruct1');
    }

}
